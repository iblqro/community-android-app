package mx.ibl.ientry.community.appinfo.community.common_area

import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface CommonAreaApi {

    @GET( "common-area/v1/all/{id}")
    fun getByResidencyID( @Path( "id") id: Long): Call<List<CommonArea>>

    @POST( "common-area/v1/create")
    fun create( @Body commonArea: CommonArea): Call<CommonArea>

    @PUT( "common-area/v1/update")
    fun update( @Body commonArea: CommonArea): Call<CommonArea>
}