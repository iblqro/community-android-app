package mx.ibl.ientry.community.ui.activity.login

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.ActivityLogInBinding

/**
 * @author ISC Luis Cornejo
 * @since Friday 30, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class LogInActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLogInBinding
    private val startForResult = registerForActivityResult( ActivityResultContracts.StartActivityForResult()){}

    companion object{
        private const val TIME_SPLASH_SCREEN = 2000L
        private val REQUIRED_PERMISSIONS = arrayOf( Manifest.permission.RECEIVE_SMS)
        private const val REQUEST_CODE_PERMISSIONS = 101
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // Start splash screen //
        Thread.sleep( TIME_SPLASH_SCREEN)
        // End splash screen //
        setTheme( R.style.Theme_Community)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView( this, R.layout.activity_log_in)
        requestPermissions()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if( requestCode == REQUEST_CODE_PERMISSIONS)
            if( !allPermissionsGranted())
                dialogSettings()
    }

    private fun dialogSettings()
    {
        AlertDialog.Builder( this, R.style.AlertDialogTheme).apply {
            setTitle( "Permissions Required")
            setMessage( "Permissions are required to app")
            setPositiveButton( R.string.lb_btn_go_to){ _, _ ->
                openSettings()
            }
        }
            .create()
            .show()
    }

    private fun openSettings()
    {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        startForResult.launch(intent)
    }

    private fun requestPermissions() = ActivityCompat.requestPermissions( this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)

    private fun allPermissionsGranted():  Boolean = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission( this.baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

}