package mx.ibl.ientry.community.ui.fragment.login.forget.changepassword

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentChangePasswordBinding
import mx.ibl.ientry.community.ui.activity.login.ForgetPasswordVM
import mx.ibl.ientry.community.ui.activity.main.MainActivity
import mx.ibl.ientry.community.util.Common


class ChangePasswordFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentChangePasswordBinding
    private lateinit var viewModelActivity: ForgetPasswordVM

    companion object{
        private const val TAG = "ChangePasswordFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModelActivity = ViewModelProvider( this).get( ForgetPasswordVM::class.java)

        requireActivity().onBackPressedDispatcher.addCallback(this){
            findNavController().popBackStack( R.id.forgetPasswordFragment, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        binding = DataBindingUtil.inflate(  inflater, R.layout.fragment_change_password, container, false)
        binding.fcpChangePassword.viewModel =  viewModelActivity

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.imgFcpBack.setOnClickListener( this)
        binding.fcpChangePassword.btnIcpConfirm.setOnClickListener( this)
    }

    private fun enterToApplication()
    {
        val intent = Intent( requireActivity(), MainActivity::class.java)
        requireActivity().startActivity( intent)
        requireActivity().overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_left)
        requireActivity().finish()
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id)
            {
                binding.fcpChangePassword.btnIcpConfirm.id -> {
                    if( viewModelActivity.isOkToChangePassword())
                    {
                        viewModelActivity.updatePassword()
                        enterToApplication()
                    }
                    else
                        Common.showSnackBar( binding.root, getString( R.string.lb_pf_password_empty), requireContext())
                }
                binding.imgFcpBack.id -> findNavController().popBackStack( R.id.log_in_nav, false)
                else -> Log.e( TAG, "ID: ${it.id} Unknown")
            }
        }
    }
}