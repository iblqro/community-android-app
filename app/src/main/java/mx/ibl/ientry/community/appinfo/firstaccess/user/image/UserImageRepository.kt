package mx.ibl.ientry.community.appinfo.firstaccess.user.image

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.network.ServiceGenerator

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class UserImageRepository( context: Context) {

    private val userRepository: UserRepository by lazy { UserRepository( context) }
    private val dao: UserImageDao by lazy { AppDatabase.getInstance( context).userImageDao() }
    private val api: UserImageApi by lazy {  ServiceGenerator.createService( context, UserImageApi::class.java, userRepository.token)}

    fun getImageByIDLiveData( id: Long): LiveData<UserImage?> = dao.getImageByIDLiveData( id)
    fun getIDsImageByUserIDLiveData( id: Long, idAccessMethod: Long): LiveData<List<Long>> =  dao.getIDsImageByUserIDLiveData( id, idAccessMethod)
    fun getImagesByUserIDLiveData( id: Long, idAccessMethod: Long): LiveData<List<UserImage>> = dao.getImagesByUserIDLiveData( id, idAccessMethod)

}