package mx.ibl.ientry.community.appinfo.community.community.worker

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Friday 25, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "worker_community")
data class WorkerCommunity(
    @PrimaryKey
    val id: Long,
    val user_id: Long,
    val status_id: Long,
    val work_type_id: Long,
    val name: String?,
    val community_id: Long,
    val residency_id: Long?,
    val observation: String?,
    val created_at: Date = Common.date(),
    val update_at: Date?
)
