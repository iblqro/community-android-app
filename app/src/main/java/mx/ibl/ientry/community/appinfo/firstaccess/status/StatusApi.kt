package mx.ibl.ientry.community.appinfo.firstaccess.status

import retrofit2.http.GET

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface StatusApi {

    @GET( "status/v1/all")
    suspend fun getAll(): List<Status>
}