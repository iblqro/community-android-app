package mx.ibl.ientry.community.ui.list.vote

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.appinfo.community.community.approveversion.Vote
import mx.ibl.ientry.community.databinding.ItemUserVoteBinding

/**
 * @author ISC Luis Cornejo
 * @since Friday 25, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class VoteAdapter: RecyclerView.Adapter<VoteVH>() {

    private var list = listOf<Vote>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VoteVH {
        this.context =  parent.context

        return VoteVH( ItemUserVoteBinding.inflate( LayoutInflater.from(this.context), parent, false))
    }

    override fun onBindViewHolder(holder: VoteVH, position: Int) {
        val item =  this.list[position]
        val view =  holder.view

        view.lbIuvTitle.text = item.title
        view.lbIuvSubtitle.text = item.subtitle

        when( item.value){
            null -> view.imgIuvStatus.setImageDrawable( ContextCompat.getDrawable( this.context,  R.drawable.ic_minus))
            true -> view.imgIuvStatus.setImageDrawable( ContextCompat.getDrawable( this.context,  R.drawable.ic_check))
            else -> view.imgIuvStatus.setImageDrawable( ContextCompat.getDrawable( this.context,  R.drawable.ic_clear))
        }

    }

    override fun getItemCount(): Int = list.size

    fun setList( list: List<Vote>)
    {
        this.list =  list
    }
}