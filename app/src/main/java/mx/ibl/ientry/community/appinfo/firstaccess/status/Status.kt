package mx.ibl.ientry.community.appinfo.firstaccess.status

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "status")
data class Status(
    @PrimaryKey
    val id: Long,
    val name: String,
    val created_at: Date,
    val updated_at: Date?
)
{
    override fun toString(): String {
        return name
    }
}