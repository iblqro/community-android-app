package mx.ibl.ientry.community.appinfo.community.community.image

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Friday 30, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface CommunityImageApi {

    @GET( "urbanization-image/v1/{id}")
    fun getByVehicleID( @Path( "id") id: Long): Call<List<CommunityImage>>

    @Multipart
    @POST( "urbanization-image/v1/create")
    fun create(
        @Part( "file") image: MultipartBody.Part,
        @Part( "urbanization-image") communityImage: CommunityImage
    ): Call<CommunityImage>
}