package mx.ibl.ientry.community.util.emun

/**
 * @author ISC Luis Cornejo
 * @since Friday 16, July 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
enum class Permission( val value: Long) {

    JUNTAS( 1),
    LINEAMIENTOS_REGULACIONES( 2),
    COMMUNITY_DATA(3),

}