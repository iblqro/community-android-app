package mx.ibl.ientry.community.appinfo.community.residence

import androidx.lifecycle.LiveData
import androidx.room.*
import mx.ibl.ientry.community.appinfo.community.catalog.residence_vehicle.ResidenceVehicle

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface ResidenceDao {
    // residence //
    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert(residences: List<Residence>): List<Long>

    @Insert
    fun insert(residence: Residence): Long

    @Update
    fun update(residence: Residence)

    @Query( "DELETE FROM residence")
    fun deleteAll()

    @Query( "SELECT * FROM residence")
    fun getAll(): LiveData<List<Residence>>

    @Query( """
        SELECT r.id FROM residence r
        INNER JOIN user_residence ur ON ur.residence_id = r.id
        WHERE ur.internal_user_id = :idUser
    """)
    fun gelAllIDsLiveData( idUser: Long): LiveData<List<Long>>

    @Query(
        """
                    SELECT r.id AS residence_id, r.name AS residence, r.address, r.exterior_number, r.interior_number,
                    uo.name AS owner, uo.internal_id AS owner_id,
                    u.name AS community,
                    s.name AS status,
                    ( SELECT 1) AS no_vehicles,
                    ( SELECT 1) AS no_occupants
                    FROM residence r
                    INNER JOIN user uo ON r.internal_user_id = uo.internal_id
                    INNER JOIN community u ON u.id = r.community_id
                    INNER JOIN status s ON s.id =  r.status_id
                    WHERE r.id = :id
            """)
    fun getResidenceItemLiveData(id: Long): LiveData<ResidenceItem?>

    // Residence Vehicle //
    @Insert
    fun insert( residenceVehicle: ResidenceVehicle): Long

    @Update
    fun update( residenceVehicle: ResidenceVehicle)

    @Query(
        """
        SELECT * FROM residence_vehicle WHERE id = 0 AND internal_vehicle_id > 0 AND id_internal_user > 0
    """
    )
    fun toUpload(): List<ResidenceVehicle>

    @Query(
        """
        SELECT * FROM residence_vehicle WHERE vehicle_id = 0 AND internal_vehicle_id = :id
    """
    )
    fun toUpdateVehicleID( id: Long): List<ResidenceVehicle>

    @Query( """
        SELECT * FROM residence_vehicle WHERE id_user = 0 AND id_internal_user = :id
    """)
    fun toUpdateUserID( id: Long): List<ResidenceVehicle>

    // Residence Urbanization //
}