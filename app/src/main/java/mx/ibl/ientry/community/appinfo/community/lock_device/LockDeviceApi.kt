package mx.ibl.ientry.community.appinfo.community.lock_device

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface LockDeviceApi {

    @GET( "lock-device/v1/urbanization/{id}")
    fun getByUrbanizationID( @Path( "id") id: Long): Call<List<LockDevice>>

    @GET( "lock-device/v1/residency/{id}")
    fun getByResidencyID( @Path( "id") id: Long): Call<List<LockDevice>>
}