package mx.ibl.ientry.community.appinfo.firstaccess.gender

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface GenderDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( genders: List<Gender>): List<Long>

    @Query( "DELETE FROM gender")
    fun deleteAll()

    @Query( """
        SELECT name FROM gender WHERE id = :id
    """)
    fun getGenderNameByID( id: Long ): String

    @Query( """
        SELECT * FROM gender
    """)
    fun getAllLiveData(): LiveData<List<Gender>>
}