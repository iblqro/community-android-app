package mx.ibl.ientry.community.appinfo.community.tag

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface TagDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( tags: List<Tag>): List<Long>

    @Insert
    fun insert( tag: Tag): Long

    @Update
    fun update( tag: Tag)

    @Query( "DELETE FROM tag")
    fun deleteAll()

    @Query("SELECT * FROM tag WHERE community_id = :id")
    fun getTagByUrbanizationID( id: Long): LiveData<List<Tag>>

    @Query("SELECT * FROM tag WHERE community_id = :id AND status_id = :idStatus")
    fun getTagByUrbanizationIDAndStatusID( id: Long, idStatus: Long): LiveData<List<Tag>>
}