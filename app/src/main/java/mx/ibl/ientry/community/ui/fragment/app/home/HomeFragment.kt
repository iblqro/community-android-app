package mx.ibl.ientry.community.ui.fragment.app.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentHomeBinding
import mx.ibl.ientry.community.ui.activity.main.MainVM
import mx.ibl.ientry.community.util.emun.Option

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeVM
    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModelActivity: MainVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( MainVM::class.java)
        viewModel = ViewModelProvider(this).get(HomeVM::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModelActivity.setOption( Option.HOME)
    }
}