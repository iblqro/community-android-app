package mx.ibl.ientry.community.appinfo.community.tag

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "tag")
data class Tag(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val id: Long = 0,
    val status_id: Long,
    val tag: String,
    val community_id: Long?,
    val created_at: Date = Common.date(),
)