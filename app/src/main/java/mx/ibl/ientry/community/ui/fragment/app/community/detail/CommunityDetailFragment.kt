package mx.ibl.ientry.community.ui.fragment.app.community.detail

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentCommunityDetailBinding
import mx.ibl.ientry.community.ui.activity.main.MainVM
import mx.ibl.ientry.community.util.emun.Option

class CommunityDetailFragment : Fragment(), MenuItem.OnMenuItemClickListener {

    private lateinit var binding: FragmentCommunityDetailBinding
    private lateinit var viewModelActivity: MainVM
    private lateinit var viewModel: CommunityDetailVM
    private lateinit var factory: CommunityDetailVMF
    private var id: Long = 0L

    companion object
    {
        const val COMMUNITY_ID = "community_id"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id =  it.getLong( COMMUNITY_ID, 0)
        }
        viewModelActivity =  ViewModelProvider( requireActivity()).get( MainVM::class.java)
        viewModelActivity.setOption( Option.NONE)

        factory = CommunityDetailVMF( requireActivity().application, id)
        viewModel =  ViewModelProvider( this, factory).get( CommunityDetailVM::class.java)
        registerForContextMenu( binding.imgFcdMenu)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_community_detail, container, false)
        binding.viewModel = viewModel


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menu.add( R.string.lb_menu_community_meetings).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_regulations).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_update_data).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_common_areas).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_worker).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_comitee).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_administrator).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_reports_charts).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_request_residentes).setOnMenuItemClickListener(this)
        menu.add( R.string.lb_menu_community_idle_vehicles).setOnMenuItemClickListener(this)
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        item?.let {
            when( it.title)
            {
                getString( R.string.lb_menu_community_meetings) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_meetings), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_regulations) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_regulations), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_update_data) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_update_data), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_common_areas) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_common_areas), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_worker) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_worker), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_comitee) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_comitee), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_administrator) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_administrator), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_reports_charts) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_reports_charts), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_request_residentes) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_request_residentes), Toast.LENGTH_LONG).show()
                }
                getString( R.string.lb_menu_community_idle_vehicles) -> {
                    Toast.makeText( requireContext(), getString(R.string.lb_menu_community_idle_vehicles), Toast.LENGTH_LONG).show()
                }
            }
            return true
        }?: kotlin.run {
            return false
        }
    }
}