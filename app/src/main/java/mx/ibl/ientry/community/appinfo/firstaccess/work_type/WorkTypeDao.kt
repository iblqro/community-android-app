package mx.ibl.ientry.community.appinfo.firstaccess.work_type

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface WorkTypeDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( workTypes: List<WorkType>)

    @Query( "DELETE FROM work_type")
    fun deleteAll()

    @Query( "SELECT * FROM work_type")
    fun getAll(): List<WorkType>
}