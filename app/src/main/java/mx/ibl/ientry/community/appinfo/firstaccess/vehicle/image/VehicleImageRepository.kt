package mx.ibl.ientry.community.appinfo.firstaccess.vehicle.image

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.network.ServiceGenerator

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class VehicleImageRepository( context: Context) {

    private val userRepository: UserRepository by lazy { UserRepository( context) }
    private val dao: VehicleImageDao by lazy { AppDatabase.getInstance( context).vehicleImageDao() }
    private val api: VehicleImageApi by lazy { ServiceGenerator.createService( context, VehicleImageApi::class.java, userRepository.token) }

    fun getImagesByVehicleIDLiveData( id: Long): LiveData<List<VehicleImage>> =  dao.getImagesByVehicleIDLiveData( id)
}