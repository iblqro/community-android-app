package mx.ibl.ientry.community.util

import android.content.Context
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.text.style.StyleSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import mx.ibl.ientry.community.R
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class Common {

    companion object {
        private val localeEsMX = Locale.forLanguageTag("es-MX")

        val format = SimpleDateFormat("MMMM yyyy", localeEsMX)
        val dateSlashFormat = SimpleDateFormat( "dd/MM/yyyy", localeEsMX)
        val dayFormat = SimpleDateFormat("EEEE dd", localeEsMX)
        val onlyDayStringFormat = SimpleDateFormat("dd EEEE", localeEsMX)
        val onlyDayFormat = SimpleDateFormat("dd", localeEsMX)
        val hourFormat = SimpleDateFormat("HH:mm a", localeEsMX)
        val onlyHourFormat = SimpleDateFormat("HH:mm", localeEsMX)
        val decimalFormat = DecimalFormat("###,###,###.##")
        val bss = StyleSpan( android.graphics.Typeface.BOLD)
        val iss = StyleSpan( android.graphics.Typeface.ITALIC)

        fun snackBar( view: View, message: String, time: Int) = Snackbar.make( view, message, time)

        fun capitalLetter(word: String): String = word.substring(0, 1)
                .toUpperCase(Locale.forLanguageTag("es-MX")) + word.substring(1)
                .toLowerCase(Locale.forLanguageTag("es-MX"))

        fun date(): Date = Calendar.getInstance( localeEsMX).time

        fun calendar(): Calendar = Calendar.getInstance( localeEsMX)

        fun dateInMillis(): Long =
            Calendar.getInstance( localeEsMX).timeInMillis

        fun getVerticalManager(context: Context): RecyclerView.LayoutManager =
            if (context.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                GridLayoutManager(context, 2) else LinearLayoutManager(context)

        fun getHorizontalManager(context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        fun isWiFiConnection(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Comun", "conexion a WIFI")
                    return true
                }
                Log.i("Comun", "conexion a red")
                return false
            }
            Log.i("Comun", "Sin conexion a red")
            return false
        }

        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    return true
                }
            }
            return false
        }
        fun isEmailValid( email: String): Boolean = Patterns.EMAIL_ADDRESS.matcher( email).matches()

        fun isPhoneNumberValid( number: String): Boolean = number.length == 10

        fun hideKeyboard( context: Context,  v: View){
            val imm = context.getSystemService( Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow( v.windowToken, 0)
        }

        fun showSnackBar(view: View, message: String, context: Context)
        {
            Snackbar.make( view, message,Snackbar.LENGTH_LONG)
                .setTextColor( ContextCompat.getColor( context, R.color.white))
                .setBackgroundTint( ContextCompat.getColor( context, R.color.community_accent))
                .show()
        }
        /*
        Register events in calendar
        // Event is on January 23, 2021 -- from 7:30 AM to 10:30 AM.
        Intent(Intent.ACTION_INSERT, Events.CONTENT_URI).apply {
        val beginTime: Calendar = Calendar.getInstance().apply {
            set(2021, 0, 23, 7, 30)
        }
        val endTime = Calendar.getInstance().apply {
            set(2021, 0, 23, 10, 30)
        }
        putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.timeInMillis)
        putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.timeInMillis)
        putExtra(Events.TITLE, "Ninja class")
        putExtra(Events.EVENT_LOCATION, "Secret dojo")
        startActivity(intent)
         */
    }
}
