package mx.ibl.ientry.community.network.errormanager

/**
 * @author ISC Luis Cornejo
 * @since Tuesday 04, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
sealed class ResultWrapper<out T>{
    data class Success<out T>(val value: T): ResultWrapper<T>()
    data class GenericError(val code: Int? = null, val error: String? = null): ResultWrapper<Nothing>()
    object NetworkError: ResultWrapper<Nothing>()
}
