package mx.ibl.ientry.community.appinfo.community.catalog.residence_vehicle

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author ISC Luis Cornejo
 * @since Wednesday 05, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "residence_vehicle")
data class ResidenceVehicle(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val id: Long = 0,
    val residence_id: Long,
    val internal_vehicle_id: Long,
    val vehicle_id: Long = 0,
    val id_internal_user: Long,
    val id_user: Long = 0,
    val id_tag: Long?
)