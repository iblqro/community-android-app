package mx.ibl.ientry.community.ui.fragment.app.community

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.community.community.Community
import mx.ibl.ientry.community.appinfo.community.community.CommunityDetail
import mx.ibl.ientry.community.appinfo.community.community.CommunityRepository
import mx.ibl.ientry.community.appinfo.community.community.approveversion.Vote

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommunityVM(private val app: Application) : AndroidViewModel( app) {

    private val communityRepository: CommunityRepository by lazy { CommunityRepository( app) }

    val communities: LiveData<List<Long>> =  communityRepository.getAllCommunities()

    fun getCommunityByID( id: Long): LiveData<Community?> =  communityRepository.getByIDCommunity( id)
    fun getApproveCommunityVersionByCommunityID( id: Long): LiveData<List<Vote>> = communityRepository.getApproveCommunityVersionByCommunityID( id)
    fun getUrbanizationDetailByCommunityID( id: Long): LiveData<CommunityDetail?> = communityRepository.getCommunityDetailByCommunityID( id)
}