package mx.ibl.ientry.community.ui.fragment.login.forget.forgetpassword

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentForgetPasswordBinding
import mx.ibl.ientry.community.ui.activity.login.ForgetPasswordVM
import mx.ibl.ientry.community.util.Common

class ForgetPasswordFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentForgetPasswordBinding
    private lateinit var viewModelActivity: ForgetPasswordVM

    companion object{
        private const val TAG = "ForgetPasswordFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( ForgetPasswordVM::class.java)
        viewModelActivity.email.value = ""
        viewModelActivity.option = null
        viewModelActivity.user = null

        requireActivity().onBackPressedDispatcher.addCallback(this){
            findNavController().popBackStack()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =  DataBindingUtil.inflate( inflater, R.layout.fragment_forget_password, container, false)
        binding.ffpForgetPassword.viewModel = viewModelActivity

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.imgFfpBack.setOnClickListener( this)
        binding.ffpForgetPassword.btnIfpConfirm.setOnClickListener( this)
        binding.ffpForgetPassword.btnIfpCancel.setOnClickListener( this)
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id){
                binding.ffpForgetPassword.btnIfpConfirm.id -> {
                    if( viewModelActivity.emailNotEmpty())
                    {
                        if( viewModelActivity.emailNotValid())
                        {
                            if( viewModelActivity.checkEmailIsValid())
                            {
                                viewModelActivity.getUserFromServer()
                                findNavController().navigate( R.id.action_forgetPasswordFragment_to_selectMethodFragment)
                            }
                            else
                                Common.showSnackBar( binding.root, getString( R.string.lb_email_not_exists), requireContext())

                        }
                        else
                            Common.showSnackBar( binding.root, getString( R.string.lb_email_not_valid), requireContext())
                    }
                    else
                        Common.showSnackBar( binding.root, getString( R.string.lb_email_empty), requireContext())
                }
                binding.ffpForgetPassword.btnIfpCancel.id -> findNavController().popBackStack()
                binding.imgFfpBack.id -> findNavController().popBackStack()
                else -> { Log.e( TAG, "ID: ${it.id} unknown")}
            }
        }
    }
}