package mx.ibl.ientry.community.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

/**
 * @author ISC Luis Cornejo
 * @since Tuesday 04, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class AuthenticationInterceptor(private val authToken: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder()
                .header("Authorization", authToken)
                .header("Content-Type", "application/x-www-form-urlencoded")
        val request = builder.build()
        return chain.proceed(request)
    }
}