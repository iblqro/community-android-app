package mx.ibl.ientry.community.appinfo.community.community.communityversion

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 24, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "community_version")
data class CommunityVersion(
    @PrimaryKey( autoGenerate = true)
    val id: Long,
    val community_id: Long,
    val version: Int,
    val is_approve: Boolean?,
    val approve_date: Date?,
    val created_at: Date,
    val updated_at: Date?
)
