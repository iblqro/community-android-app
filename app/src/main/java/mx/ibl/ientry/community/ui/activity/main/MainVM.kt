package mx.ibl.ientry.community.ui.activity.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import mx.ibl.ientry.community.appinfo.firstaccess.user.User
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.util.emun.Option

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class MainVM( private val app: Application): AndroidViewModel( app) {

    private val userRepository: UserRepository by lazy { UserRepository( app) }
    private val _option = MutableLiveData<Option>().apply {
        value = Option.HOME
    }
    val option: LiveData<Option> = _option
    val user: User = userRepository.user

    init {

    }

    fun setOption( value: Option) {
        _option.value = value
    }
}