package mx.ibl.ientry.community.ui.activity.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import mx.ibl.ientry.community.appinfo.community.residence.Residence
import mx.ibl.ientry.community.appinfo.community.residence.ResidenceRepository
import mx.ibl.ientry.community.appinfo.firstaccess.user.User
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserLogin
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository

/**
 * @author ISC Luis Cornejo
 * @since Friday 30, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class LogInVM( val app: Application): AndroidViewModel( app) {

    private val residenceRepository: ResidenceRepository by lazy { ResidenceRepository( app) }
    private val userRepository: UserRepository by lazy { UserRepository( app) }

    val user = MutableLiveData<UserLogin>().apply {
        value = UserLogin()
    }
    var userLogged: User? = null
    private val _timeMessage =  MutableLiveData<Int>()
    val timeMessage: LiveData<Int> = _timeMessage
    private val _message = MutableLiveData<String>()
    val message: LiveData<String> = _message
    // Asignación de residente/no residente //
    val residence = MutableLiveData<Residence?>()
    val residences = MutableLiveData<List<Residence>>().apply {
        value = listOf()
    }

    fun checkIsInputsNotEmpty(): Boolean = user.value!!.username.isNotEmpty() && user.value!!.password.isNotEmpty()

    fun setTime( time: Int){
        _timeMessage.value = time
    }

    fun setMessage( message: String){
        _message.apply {
            value = message
        }
    }

    fun setUserToLiveThere()
    {
        // ToDo: Actualizar que el usuario vivirá en esa casa
    }

    fun getResidenceToAssign()
    {
        residence.value =  residenceRepository.getResidenceToAssign()
    }

    fun getResidencesToAssign()
    {
        residences.value =  residenceRepository.getResidencesToAssign()
    }

    fun getUserFromServer(){
        userLogged =  userRepository.user
    }

    fun clearUserDataToLogIn()
    {
        user.value?.apply {
            this.username = ""
            this.password = ""
        }
    }
}