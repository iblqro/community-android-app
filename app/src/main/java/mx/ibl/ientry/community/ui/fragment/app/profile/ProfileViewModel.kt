package mx.ibl.ientry.community.ui.fragment.app.profile

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.firstaccess.user.User
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository

class ProfileViewModel( app: Application) : AndroidViewModel( app) {

    private val userRepository: UserRepository by lazy { UserRepository( app) }

    val user: LiveData<User?> = userRepository.getUserLiveData( userRepository.user.internal_id)


    fun logOut()
    {
    }
}