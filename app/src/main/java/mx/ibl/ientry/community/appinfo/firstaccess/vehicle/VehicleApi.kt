package mx.ibl.ientry.community.appinfo.firstaccess.vehicle

import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface VehicleApi {

    @GET( "vehicle/v1/all/residency/{id}")
    fun getAllVehiclesResidency( @Path( "id") id: Long): Call<List<Vehicle>>

    @POST( "vehicle/v1/create")
    fun create( @Body vehicle: Vehicle): Call<Vehicle>

    @PUT( "vehicle/v1/update")
    fun update(@Body vehicle: Vehicle): Call<Vehicle>
}