package mx.ibl.ientry.community.appinfo.community.common_area.image

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface CommonAreaImageApi {

    @GET( "common-area-image/v1/{id}")
    fun get( @Path( "id") id: Long): Call<List<CommonAreaImage>>

    @Multipart
    @POST( "common-area-image/v1/create")
    fun create(
        @Part( "file") image: MultipartBody.Part,
        @Part( "common-area-image") commonAreaImage: CommonAreaImage
    ): Call<CommonAreaImage>
}