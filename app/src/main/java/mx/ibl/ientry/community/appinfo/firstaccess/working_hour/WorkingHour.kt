package mx.ibl.ientry.community.appinfo.firstaccess.working_hour

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "working_hour")
data class WorkingHour(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val id: Long = 0,
    val id_user: Long?,
    val id_vehicle: Long?,
    val id_day: Long,
    val hour_start_time: String,
    val minute_start_time: String,
    val hour_end_time: String,
    val minute_end_time: String,
    val created_at: Date = Common.date(),
    val updated_at: Date?
)