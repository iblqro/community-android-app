package mx.ibl.ientry.community.ui.list.image

import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemImageBinding

/**
 * @author ISC Luis Cornejo
 * @since Thursday 06, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ImageVH( val view: ItemImageBinding): RecyclerView.ViewHolder( view.root)