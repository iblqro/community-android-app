package mx.ibl.ientry.community.ui.fragment.login.forget.method

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentSelectMethodBinding
import mx.ibl.ientry.community.ui.activity.login.ForgetPasswordVM
import mx.ibl.ientry.community.ui.fragment.login.forget.resetpassword.ResetPasswordFragment


class SelectMethodFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentSelectMethodBinding
    private lateinit var viewModelActivity: ForgetPasswordVM

    companion object
    {
        const val RESET_PASSWORD_OPTION = "reset_password_option"
        private const val TAG = "SelectMethodFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( ForgetPasswordVM::class.java)

        requireActivity().onBackPressedDispatcher.addCallback(this){
            findNavController().popBackStack( R.id.forgetPasswordFragment, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_select_method, container, false)
        binding.fslSelectMethod.viewModel = viewModelActivity

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.imgFfpBack.setOnClickListener( this)
        binding.fslSelectMethod.llIsmEmail.setOnClickListener( this)
        binding.fslSelectMethod.llIsmSms.setOnClickListener( this)

        binding.fslSelectMethod.lbIsmPhone.text = String.format( "********%s",
            "${viewModelActivity.user?.phone_number?.get(8)}${viewModelActivity.user?.phone_number?.get(9)}"
        )
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id){
                binding.imgFfpBack.id -> findNavController().popBackStack( R.id.log_in_nav, false)
                binding.fslSelectMethod.llIsmEmail.id -> {
                    val bundle = bundleOf( RESET_PASSWORD_OPTION to ResetPasswordFragment.IS_EMAIL)
                    findNavController().navigate( R.id.action_selectMethodFragment_to_resetPasswordFragment, bundle)
                }
                binding.fslSelectMethod.llIsmSms.id -> {
                    val bundle = bundleOf( RESET_PASSWORD_OPTION to ResetPasswordFragment.IS_SMS)
                    findNavController().navigate( R.id.action_selectMethodFragment_to_resetPasswordFragment, bundle)
                }
                else -> Log.e( TAG, "ID: ${it.id} Unknown")
            }
        }
    }
}