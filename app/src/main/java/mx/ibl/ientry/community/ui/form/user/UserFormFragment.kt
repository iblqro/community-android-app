package mx.ibl.ientry.community.ui.form.user

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.appinfo.firstaccess.gender.Gender
import mx.ibl.ientry.community.appinfo.firstaccess.status.Status
import mx.ibl.ientry.community.databinding.FragmentUserFormBinding
import mx.ibl.ientry.community.ui.activity.main.MainVM
import mx.ibl.ientry.community.util.Common
import mx.ibl.ientry.community.util.emun.Option
import java.util.*

class UserFormFragment : Fragment(), TextWatcher, DatePickerDialog.OnDateSetListener,
    View.OnFocusChangeListener {

    private lateinit var binding: FragmentUserFormBinding
    private lateinit var viewModel: UserFormVM
    private lateinit var viewModelActivity: MainVM
    private lateinit var factory: UserFormVMF
    private lateinit var picker: DatePickerDialog
    private var id: Long = 0L

    companion object {
        const val ID_USER = "id_user"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            id = it.getLong(ID_USER, 0L)
        }
        factory = UserFormVMF(requireActivity().application, id)
        viewModel = ViewModelProvider(this, factory).get(UserFormVM::class.java)
        viewModelActivity = ViewModelProvider( requireActivity()).get( MainVM::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_form, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModelActivity.setOption( Option.PROFILE)
        setListeners()

        lifecycleScope.launch(Dispatchers.Default) {
            whenCreated {
                viewModel.user.observe(viewLifecycleOwner, {
                    viewModel.setGenderID( it.gender_id)
                    viewModel.setStatusID( it.status_id)
                })
                viewModel.genders.observe(viewLifecycleOwner, {
                    val adapter = ArrayAdapter(
                        requireContext(),
                        R.layout.item_dropdown,
                        it
                    )
                    binding.txtFufGender.setAdapter(adapter)
                    binding.txtFufGender.setOnItemClickListener { parent, _, position, _ ->

                        viewModel.setGenderID( ( parent.getItemAtPosition(position) as Gender).id)
                    }
                })
                viewModel.genderID.observe(viewLifecycleOwner, {
                    if (it != 0L) {
                        binding.txtFufGender.setText( viewModel.getGenderNameByID())
                        binding.ctxtFufGender.isErrorEnabled = false
                    }
                    else
                    {
                        binding.ctxtFufGender.isErrorEnabled = true
                        binding.ctxtFufGender.error = getString( R.string.lb_error_input_empty)
                        binding.ctxtFufGender.errorIconDrawable =  ContextCompat.getDrawable( requireContext(), R.drawable.ic_right_arrow)
                    }
                })
                viewModel.listStatus.observe(viewLifecycleOwner, {
                    val adapter = ArrayAdapter(
                        requireContext(),
                        R.layout.item_dropdown,
                        it
                    )
                    binding.txtFufStatus.setAdapter(adapter)
                    binding.txtFufStatus.setOnItemClickListener { parent, _, position, _ ->
                        viewModel.setStatusID( (parent.getItemAtPosition(position) as Status).id)
                    }
                })
                viewModel.statusID.observe(viewLifecycleOwner, {
                    if (it != 0L) {
                        binding.txtFufStatus.setText( viewModel.getStatusNameByID())
                        binding.ctxtFufStatus.isErrorEnabled = false
                    }
                    else
                    {
                        binding.ctxtFufStatus.isErrorEnabled = true
                        binding.ctxtFufStatus.error = getString( R.string.lb_error_input_empty)
                        binding.ctxtFufStatus.errorIconDrawable =  ContextCompat.getDrawable( requireContext(), R.drawable.ic_right_arrow)
                    }
                })
                viewModel.dob.observe(viewLifecycleOwner, {
                    picker = DatePickerDialog(
                        requireContext(), this@UserFormFragment,
                        it.get(Calendar.YEAR),
                        it.get(Calendar.MONTH),
                        it.get(Calendar.DAY_OF_MONTH)
                    )
                    binding.txtFufDob.setText(Common.dateSlashFormat.format(it.time))
                })
                binding.txtFufDob.setOnClickListener {
                    showDatePickerDialog()
                }
                binding.btnFufSaveChanges.setOnClickListener {
                    if( checkIfNotExistsAnError())
                    {
                        viewModel.saveChanges()
                        findNavController().popBackStack()
                    }
                }
            }
        }
    }

    private fun showDatePickerDialog() {
        picker.show()
    }

    private fun setListeners() {
        binding.txtFufEmail.addTextChangedListener(this)
        binding.txtFufDob.onFocusChangeListener = this
    }

    private fun checkIfNotExistsAnError(): Boolean
    {
        viewModel.user.value?.let { user ->
            // Email input //
            if (Common.isEmailValid(user.email))
                binding.ctxtFufEmail.isErrorEnabled = false
            else {
                binding.ctxtFufEmail.isErrorEnabled = true
                binding.ctxtFufEmail.error = getString(R.string.lb_error_email_not_valid)
                return false
            }
            // Number input //
            if (Common.isPhoneNumberValid(user.phone_number))
                binding.ctxtFufPhoneNumber.isErrorEnabled = false
            else {
                binding.ctxtFufPhoneNumber.isErrorEnabled = false
                binding.ctxtFufPhoneNumber.error = getString(R.string.lb_error_number_not_valid)
                return false
            }
            // Password input //
            if (user.password.isNotEmpty()) {
                binding.ctxtFufPassword.isErrorEnabled = false
            }
            else {
                binding.ctxtFufPassword.error = getString(R.string.lb_error_input_empty)
                binding.ctxtFufPassword.isErrorEnabled = true
                return false
            }
            // First name input //
            if (user.name.isNotEmpty()) {
                binding.ctxtFufName.isErrorEnabled = false
            }
            else {
                binding.ctxtFufName.error = getString(R.string.lb_error_input_empty)
                binding.ctxtFufName.isErrorEnabled = true
                return false
            }
            return true
        }?: kotlin.run {
            return false
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Common.calendar()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        viewModel.setDob(calendar)
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        v?.let {
            when (it.id) {
                binding.txtFufDob.id -> {
                    if (!hasFocus) Common.hideKeyboard(requireContext(), it)
                }
                else -> {
                }
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }
}