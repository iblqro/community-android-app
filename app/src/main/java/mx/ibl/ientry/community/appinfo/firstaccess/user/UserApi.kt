package mx.ibl.ientry.community.appinfo.firstaccess.user

import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface UserApi {

    @GET( "user/v1/{id}")
    fun getUserByID( @Path( "id") id: Long): Call<User>

    @POST( "user/v1/username/password")
    suspend fun get( @Body body: HashMap<String, Any>): User

    @POST( "user/v1/create")
    suspend fun create( @Body body: HashMap<String, Any>): User

    @PUT( "user/v1/update")
    fun update( @Body body: HashMap<String, Any>): Call<User>
}