package mx.ibl.ientry.community.appinfo.community.community.approveversion

/**
 * @author ISC Luis Cornejo
 * @since Friday 25, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
data class Vote(
    val title: String,
    val subtitle: String,
    val value: Boolean?
)
