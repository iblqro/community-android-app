package mx.ibl.ientry.community.ui.list.common_area

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemCommonAreaBinding

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommonAreaAdapter( val activiy: ViewModelStoreOwner): RecyclerView.Adapter<CommonAreaVH>() {

    private lateinit var context: Context;
    private lateinit var navController: NavController
    private var list = listOf<Long>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommonAreaVH {
        context = parent.context
        navController = parent.findNavController()

        return CommonAreaVH( ItemCommonAreaBinding.inflate( LayoutInflater.from( context), parent, false))
    }

    override fun onBindViewHolder(holder: CommonAreaVH, position: Int) {
        val binding = holder.view
        val id = list[position]
    }

    override fun getItemCount(): Int = list.size

    fun setList( aux: List<Long>)
    {
        list = aux
    }
}