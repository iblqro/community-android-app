package mx.ibl.ientry.community.util.emun

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
enum class Option {
    HOME, PENALTY, CONSTRUCTION, PAYMENT, PROFILE, NONE, ONLY_BAR
}