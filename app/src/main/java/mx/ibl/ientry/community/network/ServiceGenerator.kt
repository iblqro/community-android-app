package mx.ibl.ientry.community.network

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import mx.ibl.ientry.community.network.adapter.BooleanTypeAdapter
import mx.ibl.ientry.community.network.adapter.DateTypeAdapter
import mx.ibl.ientry.community.network.errormanager.ResultWrapper
import mx.ibl.ientry.community.network.interceptor.AuthenticationInterceptor
import mx.ibl.ientry.community.network.interceptor.PermissionInterceptor
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * @author ISC Luis Cornejo
 * @since Wednesday 28, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
object ServiceGenerator {

    private const val BASE_URL = "https://localhost:8080/ientry/community"
    private val interceptor = HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.HEADERS

    }
    private val httpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(25, TimeUnit.SECONDS)

    private val gsonBuilder: Gson = GsonBuilder()
            .registerTypeAdapter( Boolean::class.java, BooleanTypeAdapter())
            .registerTypeAdapter( Date::class.java, DateTypeAdapter().nullSafe())
            .create()

    private val builder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
    private var retrofit = builder.build()

    fun <T> createService(context: Context, service: Class<T>, clientId: String, password: String): T {
        if (clientId.isNotEmpty() && password.isNotEmpty()) {
            // To apply SSL
            // httpClient.sslSocketFactory( SSLService.getSslSocketFactory(context), SSLService.trustManager )
            val authToken = Credentials.basic(clientId, password)
            val interceptor = AuthenticationInterceptor(authToken)
            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor)
                builder.client(httpClient.build())
                retrofit = builder.build()
            }
        }
        return retrofit.create(service)
    }

    fun <T> createService(context: Context, service: Class<T>, authToken: String): T {
        if (authToken.isNotEmpty()) {
            val interceptor = PermissionInterceptor(authToken)
            // To apply SSL
            // httpClient.sslSocketFactory( SSLService.getSslSocketFactory(context), SSLService.trustManager )
            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor)
                builder.client(httpClient.build())
                retrofit = builder.build()
            }
        }
        return retrofit.create(service)
    }

    suspend fun <T> safeApiCall(dispatcher: CoroutineDispatcher, apiCall: suspend () -> T): ResultWrapper<T> {
        return withContext(dispatcher) {
            try {
                ResultWrapper.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException -> ResultWrapper.NetworkError
                    is HttpException -> {
                        val code = throwable.code()
                        val errorResponse = throwable.response()
                        errorResponse?.let {
                            it.errorBody()?.let {
                                return@withContext ResultWrapper.GenericError(code, it.source().toString())
                            }
                        }
                        ResultWrapper.GenericError(code, errorResponse.toString())
                    }
                    else ->
                        ResultWrapper.GenericError(null, throwable.toString())
                }
            }
        }
    }
}