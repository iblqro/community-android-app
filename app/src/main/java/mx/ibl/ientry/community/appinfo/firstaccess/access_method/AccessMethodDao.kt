package mx.ibl.ientry.community.appinfo.firstaccess.access_method

import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface AccessMethodDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( accessMethods: List<AccessMethod>): List<Long>

    @Query( "DELETE FROM access_method")
    fun deleteAll()

}