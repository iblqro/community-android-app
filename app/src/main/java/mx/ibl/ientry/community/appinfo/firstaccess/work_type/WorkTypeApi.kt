package mx.ibl.ientry.community.appinfo.firstaccess.work_type

import androidx.room.Dao
import retrofit2.http.GET

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface WorkTypeApi {

    @GET( "work-type/v1/all")
    suspend fun getAll(): List<WorkType>
}