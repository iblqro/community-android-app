package mx.ibl.ientry.community.appinfo.community.common_area.image

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface CommonAreaImageDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( commonAreaImages: List<CommonAreaImage>): List<Long>

    @Insert
    fun insert( commonAreaImage: CommonAreaImage): Long

    @Update
    fun update( commonAreaImage: CommonAreaImage)

    @Query( "DELETE FROM common_area_image")
    fun deleteAll()

    @Query( """
        SELECT * FROM common_area_image WHERE id_common_area = :id
    """)
    fun getImagesByCommonAreaID( id: Long): LiveData<List<CommonAreaImage>>
}