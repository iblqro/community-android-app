package mx.ibl.ientry.community.ui.fragment.login.start.password

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentPasswordBinding
import mx.ibl.ientry.community.ui.fragment.login.start.code.CodeFragment
import mx.ibl.ientry.community.util.Common

class PasswordFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentPasswordBinding
    private lateinit var viewModel: PasswordVM
    private lateinit var password: String

    companion object{
        private const val TAG = "PasswordFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider( this).get( PasswordVM::class.java)
        arguments?.let {
            password = it.getString( CodeFragment.PASSWORD, "")
        }
        viewModel.lastPassword = password

        requireActivity().onBackPressedDispatcher.addCallback( this){
            findNavController().popBackStack( R.id.log_in_nav, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_password, container, false)
        binding.fpPassword.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fpPassword.btnIpConfirm.setOnClickListener( this)
        binding.imgFpBack.setOnClickListener( this)
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id)
            {
                binding.fpPassword.btnIpConfirm.id ->{
                    if( viewModel.isOkToChangePassword())
                    {
                        viewModel.updatePassword()
                        findNavController().navigate( R.id.action_passwordFragment_to_messageFragment)
                    }
                    else
                        Common.showSnackBar( binding.root, getString( R.string.lb_pf_password_empty), requireContext())
                }
                binding.imgFpBack.id -> findNavController().popBackStack( R.id.log_in_nav, false)
                else -> Log.e( TAG, "Error: Not view selected")
            }
        }
    }
}