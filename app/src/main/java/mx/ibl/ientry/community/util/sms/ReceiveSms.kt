package mx.ibl.ientry.community.util.sms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import androidx.lifecycle.MutableLiveData
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.ui.fragment.login.start.code.CodeFragment

/**
 * @author ISC Luis Cornejo
 * @since Tuesday 22, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ReceiveSms: BroadcastReceiver() {

    private val regexNumber = "\\s\\d{6}[.]".toRegex()
    val code = MutableLiveData<String>()
    val error = MutableLiveData<String>()

    override fun onReceive(context: Context?, intent: Intent?) {
        code.value = ""
        context?.let { c ->
            intent?.let { i ->
                if( i.action.equals(CodeFragment.SMS_ACTION)){
                    val bundle = i.extras
                    val messages =  arrayListOf<SmsMessage>()
                    var number = ""
                    var message = ""
                    if( bundle != null){
                        try {
                            val pdus = bundle["pdus"] as Array<*>?
                            pdus?.forEachIndexed { index, element ->
                                messages.add( index, SmsMessage.createFromPdu( element as ByteArray?, bundle.getString("format")))
                                number = messages[index].originatingAddress.toString()
                                message += messages[index].messageBody
                            }
                            val value =  regexNumber.find(message)
                            if( value != null && number == CodeFragment.SMS_NUMBER)
                                code.value = value.value.replace( " ", "").replace( ".", "")
                            else
                               error.value = c.getString( R.string.lb_sms_read_error)
                        }
                        catch ( e: Exception){
                            error.value = c.getString( R.string.lb_sms_read_error)
                        }
                    }
                }
            }
        }
    }
}