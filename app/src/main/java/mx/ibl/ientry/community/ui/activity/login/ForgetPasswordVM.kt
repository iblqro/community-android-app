package mx.ibl.ientry.community.ui.activity.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import mx.ibl.ientry.community.appinfo.firstaccess.user.User
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.util.Common

/**
 * @author ISC Luis Cornejo
 * @since Wednesday 23, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ForgetPasswordVM( app: Application): AndroidViewModel( app) {

    private val userRepository: UserRepository by lazy { UserRepository( app) }

    // Recuperar Contraseña //
    private val _timeMessage =  MutableLiveData<Int>()
    val timeMessage: LiveData<Int> = _timeMessage
    private val _message = MutableLiveData<String>()
    val message: LiveData<String> = _message
    // Email de recuperación //
    var user: User? = null
    val email = MutableLiveData<String>().apply {
        value = ""
    }
    // Opciones de recuperación //
    var option: String? = null
    // Actualizar contraseña //
    val password = MutableLiveData<String>().apply {
        value = ""
    }

    fun emailNotEmpty(): Boolean = email.value!!.isNotEmpty()
    fun emailNotValid(): Boolean = Common.isEmailValid( email.value!!)
    fun checkEmailIsValid(): Boolean = true
    fun getUserFromServer(){
        user =  userRepository.user
    }
    fun setTime( time: Int){
        _timeMessage.value = time
    }

    fun setMessage( message: String){
        _message.apply {
            value = message
        }
    }
    fun isOkToChangePassword(): Boolean = password.value!!.isNotEmpty()
    fun checkIsTheSameCode(): Boolean = true
    fun updatePassword()
    {

    }

    fun reSendEmail()
    {

    }

}