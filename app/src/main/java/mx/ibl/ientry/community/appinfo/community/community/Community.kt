package mx.ibl.ientry.community.appinfo.community.community

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "community")
data class Community(
    @PrimaryKey
    val id: Long,
    val status_id: Long,
    val name: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val community_id: Long?,
    val created_at: Date,
    val show_to_admin: Boolean,
    val updated_at: Date?
)