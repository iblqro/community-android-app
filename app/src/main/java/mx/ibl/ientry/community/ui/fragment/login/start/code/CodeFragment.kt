package mx.ibl.ientry.community.ui.fragment.login.start.code

import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentCodeBinding
import mx.ibl.ientry.community.ui.activity.login.LogInVM
import mx.ibl.ientry.community.util.Common
import mx.ibl.ientry.community.util.sms.ReceiveSms


class CodeFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentCodeBinding
    private lateinit var viewModelActivity: LogInVM
    private lateinit var receiveSms: ReceiveSms

    companion object {
        private const val TAG = "ChangePasswordFragment"
        const val SMS_ACTION = "android.provider.Telephony.SMS_RECEIVED"
        const val SMS_NUMBER = "6505551212"
        const val PASSWORD = "password"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider(requireActivity()).get(LogInVM::class.java)
        receiveSms = ReceiveSms()
        viewModelActivity.setTime(60)
        viewModelActivity.setMessage( "")
        requireActivity().registerReceiver(receiveSms, IntentFilter(SMS_ACTION))

        requireActivity().onBackPressedDispatcher.addCallback( this){
            findNavController().popBackStack( R.id.log_in_nav, false)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().unregisterReceiver(receiveSms)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_code, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.fcCode.lbIcNotIncoming.setOnClickListener(this)
        binding.imgFcBack.setOnClickListener(this)

        viewModelActivity.timeMessage.observe(viewLifecycleOwner, {
            lifecycleScope.launch(Dispatchers.Default){
                withContext( Dispatchers.Main){
                    if ( it > 0)
                    {
                        binding.fcCode.lbIcTime.text = String.format( " %s s.", it )
                        binding.fcCode.lbIcTimeMesage.visibility = View.VISIBLE
                    }
                    else {
                        binding.fcCode.lbIcTime.setTextColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.red
                            )
                        )
                        binding.fcCode.lbIcTimeMesage.visibility = View.GONE
                        binding.fcCode.lbIcTime.text = getString(R.string.lb_fcp_time_out)
                    }
                }
                delay(1000)
                withContext( Dispatchers.Main){
                    if( it > 0 ) viewModelActivity.setTime( it - 1)
                }
            }

        })
        viewModelActivity.message.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                lifecycleScope.launch(Dispatchers.Default) {
                    delay(100)
                    withContext(Dispatchers.Main){
                        binding.fcCode.txtIcNumber1.setText(it[0].toString())
                    }
                    delay(100)
                    withContext(Dispatchers.Main){
                        binding.fcCode.txtIcNumber2.setText(it[1].toString())
                    }
                    delay(100)
                    withContext(Dispatchers.Main){
                        binding.fcCode.txtIcNumber3.setText(it[2].toString())
                    }
                    delay(100)
                    withContext(Dispatchers.Main){
                        binding.fcCode.txtIcNumber4.setText(it[3].toString())
                    }
                    delay(100)
                    withContext(Dispatchers.Main){
                        binding.fcCode.txtIcNumber5.setText(it[4].toString())
                    }
                    delay(100)
                    withContext(Dispatchers.Main){
                        binding.fcCode.txtIcNumber6.setText(it[5].toString())
                    }
                    delay(350)
                    withContext(Dispatchers.Main){
                        val bundle = bundleOf( PASSWORD to viewModelActivity.user.value!!.password)
                        findNavController().navigate( R.id.action_CodeFragment_to_passwordFragment, bundle)
                    }
                }
            }
            else
            {
                binding.fcCode.txtIcNumber1.setText( "")
                binding.fcCode.txtIcNumber2.setText( "")
                binding.fcCode.txtIcNumber3.setText( "")
                binding.fcCode.txtIcNumber4.setText( "")
                binding.fcCode.txtIcNumber5.setText( "")
                binding.fcCode.txtIcNumber6.setText( "")
            }
        })
        binding.fcCode.lbIcUser.text = String.format(
            getString(R.string.lb_fcp_welcome),
            viewModelActivity.user.value?.username ?: getString(R.string.lb_guest)
        )
        receiveSms.code.observe(viewLifecycleOwner, {
            viewModelActivity.setMessage( it)
        })
        receiveSms.error.observe( viewLifecycleOwner, {
            Common.showSnackBar( binding.root, it, requireContext())
        })
    }


    override fun onClick(v: View?) {
        v?.let {
            when (it.id) {
                binding.fcCode.lbIcNotIncoming.id -> {
                    if( viewModelActivity.timeMessage.value!! > 0)
                        Common.showSnackBar( binding.root, getString(R.string.lb_sms_not_error), requireContext())
                    else
                    {
                        binding.fcCode.lbIcTime.setTextColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.gray
                            )
                        )
                        viewModelActivity.setTime(60)
                    }
                }
                binding.imgFcBack.id -> findNavController().popBackStack( R.id.log_in_nav, false)
                else -> Log.e(TAG, "Error: Not view selected")
            }
        }
    }

}