package mx.ibl.ientry.community.appinfo.firstaccess.permission

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "permission")
data class Permission(
    @PrimaryKey
    val id: Long,
    val name: String,
    val description: String,
    val created_at: Date,
    val updated_at: Date?
)