package mx.ibl.ientry.community.appinfo.community.community

/**
 * @author ISC Luis Cornejo
 * @since Friday 25, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
data class CommunityDetail(
    val id: Long,
    val status_id: Long,
    val name: String,
    val address: String,
    val community_id: String?,
    val father_name: String?
)
