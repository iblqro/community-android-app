package mx.ibl.ientry.community.appinfo.firstaccess.permission

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface PermissionDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( permissions: List<Permission>): List<Long>

    @Query( "DELETE FROM permission")
    fun deleteALL()

    @Query( """
        SELECT * FROM permission
    """)
    fun getAll(): LiveData<List<Permission>>
}