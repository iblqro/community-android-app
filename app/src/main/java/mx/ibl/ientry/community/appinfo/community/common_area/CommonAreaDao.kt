package mx.ibl.ientry.community.appinfo.community.common_area

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface CommonAreaDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( commonAreas: List<CommonArea>): List<Long>

    @Insert
    fun insert( commonArea: CommonArea): Long

    @Update
    fun update( commonArea: CommonArea)

    @Query(
        """
        SELECT * FROM common_area WHERE community_id = :id
    """
    )
    fun getCommonAreasByResidencyID( id: Long): LiveData<CommonArea>

    @Query( "DELETE FROM common_area")
    fun deleteAll()
}