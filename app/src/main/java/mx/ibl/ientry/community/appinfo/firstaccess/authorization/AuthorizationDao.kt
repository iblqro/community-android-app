package mx.ibl.ientry.community.appinfo.firstaccess.authorization

import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface AuthorizationDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( authorizations: List<Authorization>): List<Long>

    @Insert
    fun insert( authorization: Authorization): Long

    @Update
    fun update( authorization: Authorization)

    @Query( "DELETE FROM authorization")
    fun deleteAll()
}