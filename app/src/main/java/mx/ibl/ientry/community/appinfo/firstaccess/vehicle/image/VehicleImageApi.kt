package mx.ibl.ientry.community.appinfo.firstaccess.vehicle.image

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface VehicleImageApi {

    @GET( "vehicle-image/v1/{id}")
    fun getByVehicleID( @Path( "id") id: Long): Call<List<VehicleImage>>

    @Multipart
    @POST( "vehicle-image/v1/create")
    fun create(
        @Part( "file") image: MultipartBody.Part,
        @Part ( "vehicle-image") vehicleImage: VehicleImage
    ): Call<VehicleImage>
}