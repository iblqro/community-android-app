package mx.ibl.ientry.community.appinfo.firstaccess.authorization

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "authorization")
data class Authorization (
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val id: Long = 0,
    val id_user_manager: Long,
    val id_user: Long?,
    val id_vehicle: Long?,
    val id_status: Long,
    val created_at: Date = Common.date(),
    val updated_at: Date?
)