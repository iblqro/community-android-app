package mx.ibl.ientry.community.ui.list.community

import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemCommunityBinding

/**
 * @author ISC Luis Cornejo
 * @since Thursday 24, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommunityVH( val view: ItemCommunityBinding): RecyclerView.ViewHolder( view.root)