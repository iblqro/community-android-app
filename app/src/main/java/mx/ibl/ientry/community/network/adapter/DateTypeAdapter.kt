package mx.ibl.ientry.community.network.adapter

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.io.IOException
import java.util.*


/**
 * @author ISC Luis Cornejo
 * @since Wednesday 28, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class DateTypeAdapter : TypeAdapter<Date?>() {
    @Throws(IOException::class)
    override fun read(`in`: JsonReader): Date {
        // this is where the conversion is performed
        return Date(`in`.nextLong())
    }

    override fun write(out: JsonWriter?, value: Date?) {
        value?.let {
            out?.value( it.time)
        }
    }
}