package mx.ibl.ientry.community.ui.fragment.login.start.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentLogInBinding
import mx.ibl.ientry.community.ui.activity.login.LogInVM
import mx.ibl.ientry.community.ui.activity.main.MainActivity
import mx.ibl.ientry.community.ui.fragment.login.start.message.MessageFragment
import mx.ibl.ientry.community.util.Common

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class LogInFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentLogInBinding
    private lateinit var viewModelActivity: LogInVM
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
    private lateinit var biometricManager: BiometricManager
    private val startForResult = registerForActivityResult( ActivityResultContracts.StartActivityForResult()) {}
    private lateinit var sharedPreferences: SharedPreferences

    companion object{
        private const val TAG = "LogInFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( LogInVM::class.java)
        sharedPreferences =  requireActivity().getSharedPreferences( getString(R.string.settings_app), Context.MODE_PRIVATE)

        requireActivity().onBackPressedDispatcher.addCallback( this){ requireActivity().finish()}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_log_in, container, false)
        viewModelActivity.clearUserDataToLogIn()

        binding.viewModel = viewModelActivity

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnFli.setOnClickListener( this)
        binding.llFliForgetPassword.setOnClickListener( this)

        biometricManager = BiometricManager.from( requireContext())
        biometricPrompt = createBiometricPrompt()
        promptInfo = createPromptInfo()

        if( viewModelActivity.userLogged != null)
        {
            binding.ctxtFliUsername.visibility = View.GONE
            binding.lbFliGreetings.visibility = View.VISIBLE
            binding.lbFliUsername.visibility = View.VISIBLE
            lifecycleScope.launch( Dispatchers.Default){
                whenCreated {
                    viewModelActivity.residences.observe( viewLifecycleOwner, {
                        Log.i( TAG, "Entro ${it.size} -- Se asigna antes de observar")
                    })
                    viewModelActivity.getResidencesToAssign()
                    viewModelActivity.getResidenceToAssign()
                }
            }
        }
        else
        {
            binding.ctxtFliUsername.visibility = View.VISIBLE
            binding.lbFliGreetings.visibility = View.GONE
            binding.lbFliUsername.visibility = View.GONE
        }

        if( viewModelActivity.userLogged != null
            && sharedPreferences.getBoolean( MessageFragment.PREFERENCES_BIOMETRIC, false))
        {
            lifecycleScope.launch( Dispatchers.Default){
                delay(500)
                withContext( Dispatchers.Main){
                    startBiometricPrompt()
                }
            }
        }


    }

    private fun createBiometricPrompt(): BiometricPrompt{

        val callback =  object : BiometricPrompt.AuthenticationCallback(){

            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                Log.e(TAG, "$errorCode -> $errString")
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Log.e(TAG, "Authentication failed for unknown reason")
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                Log.d(TAG, "Authentication was successful + ${result.cryptoObject} :: ${result.authenticationType}")
                isSomeResidenceWithoutAssign()
            }
        }

        return BiometricPrompt( this, ContextCompat.getMainExecutor( requireContext()), callback)
    }

    private fun createPromptInfo(): BiometricPrompt.PromptInfo =
        BiometricPrompt.PromptInfo.Builder().apply {
            setTitle( getString( R.string.lb_alert_login))
            // setSubtitle( "Subtitle")
            setDescription( getString( R.string.lb_alert_login_message))
            // Authenticate without requiring the user to press a "confirm"
            // false -> no button, true -> button
            // button after satisfying the biometric check
            setConfirmationRequired( true)
            setNegativeButtonText( getString( R.string.lb_btn_cancel))
            /** .setAllowedAuthenticators(
            BiometricManager.Authenticators.BIOMETRIC_STRONG or
            BiometricManager.Authenticators.BIOMETRIC_WEAK)**/
            // Also note that setAllowedAuthenticators and setNegativeButtonText are
            // incompatible so that if you uncomment one you must comment out the other
        }.build()

    private fun startBiometricPrompt(){
        when (biometricManager.canAuthenticate( BiometricManager.Authenticators.BIOMETRIC_WEAK or BiometricManager.Authenticators.BIOMETRIC_STRONG)) {
            BiometricManager.BIOMETRIC_SUCCESS ->{
                biometricPrompt.authenticate( promptInfo)
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> showAlertDialog( R.string.lb_title_error_biometric_credentials, R.string.lb_biometric_feature_unavailables)
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> showAlertDialog( R.string.lb_title_error_biometric_credentials, R.string.lb_biometric_no_hardware_message)
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
                    startEnrollAuthentication()
                else
                    showAlertDialog( R.string.lb_title_add_biometric_credentials, R.string.lb_biometric_none_enrolled_setting)
            }
            BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> showAlertDialog( R.string.lb_title_error_biometric_credentials, R.string.lb_biometric_error_security)
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> Log.e(TAG, "Biometric error unsupported.")
            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> Log.e(TAG, "Biometric status unknown.")
        }
    }

    @RequiresApi(Build.VERSION_CODES.R)
    private fun startEnrollAuthentication()
    {
        AlertDialog.Builder( requireContext(), R.style.AlertDialogTheme).apply {
            setTitle( getString( R.string.lb_title_add_biometric_credentials))
            setMessage( getString( R.string.lb_biometric_none_enrolled_setting))
            setPositiveButton( getString( R.string.lb_btn_go_to)){ _, _ ->
                // Prompts the user to create credentials that your app accepts.
                val enrollIntent = Intent(Settings.ACTION_BIOMETRIC_ENROLL).apply {
                    putExtra(Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED, BiometricManager.Authenticators.BIOMETRIC_WEAK or BiometricManager.Authenticators.BIOMETRIC_STRONG)
                }
                startForResult.launch( enrollIntent)
            }
        }.create().show()
    }

    private fun showAlertDialog( title: Int, message: Int)
    {
        AlertDialog.Builder( requireContext(), R.style.AlertDialogTheme).apply {
            setTitle( getString( title))
            setMessage( getString( message))
            setPositiveButton( getString( R.string.lb_btn_accept), null)
        }.create().show()
    }

    private fun isSomeResidenceWithoutAssign()
    {
        if( viewModelActivity.userLogged != null)
        {
            if( viewModelActivity.residences.value!!.isNotEmpty())
                findNavController().navigate( R.id.action_log_in_nav_to_manageResidenceFragment)
            else
                enterToApplication()
        }
        else
            findNavController().navigate(R.id.action_log_in_nav_to_CodeFragment)
    }

    private fun enterToApplication()
    {
        val intent = Intent( requireActivity(), MainActivity::class.java)
        requireActivity().startActivity( intent)
        requireActivity().overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_left)
        requireActivity().finish()
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id)
            {
                binding.btnFli.id -> {
                    if( viewModelActivity.checkIsInputsNotEmpty())
                        isSomeResidenceWithoutAssign()
                    else
                        Common.showSnackBar( binding.root, getString( R.string.lb_fli_empty_credentials), requireContext())

                }
                binding.llFliForgetPassword.id -> findNavController().navigate( R.id.action_log_in_nav_to_forgetPasswordFragment)
            }
        }
    }
}