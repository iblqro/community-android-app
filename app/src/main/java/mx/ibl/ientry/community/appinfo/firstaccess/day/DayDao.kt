package mx.ibl.ientry.community.appinfo.firstaccess.day

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface DayDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( days: List<Day>): List<Long>

    @Query( "DELETE FROM day")
    fun deleteAll()
}