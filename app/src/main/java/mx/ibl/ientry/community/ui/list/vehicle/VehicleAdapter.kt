package mx.ibl.ientry.community.ui.list.vehicle

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.ItemVehicleBinding
import mx.ibl.ientry.community.ui.viewmodel.GeneralVM

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class VehicleAdapter( private val activity: ViewModelStoreOwner): RecyclerView.Adapter<VehicleVH>() {

    private var list = listOf<Long>()
    private lateinit var context: Context
    private lateinit var navController: NavController
    private val viewModel: GeneralVM = ViewModelProvider( activity).get( GeneralVM::class.java)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleVH {
        context = parent.context
        navController = parent.findNavController()

        return VehicleVH( ItemVehicleBinding.inflate( LayoutInflater.from( context), parent, false))
    }

    override fun onBindViewHolder(holder: VehicleVH, position: Int) {
        val binding = holder.view
        val id = list[position]

        viewModel.getVehicleByIDLiveData( id).observe( activity as LifecycleOwner, Observer {
            it?.let { item ->
                binding.lbIvBrand.text = item.brand
                binding.lbIvColor.text = item.color
                binding.lbIvPlates.text = item.plate
                binding.lbIvStatus.text = viewModel.getStatusNameByID( item.id_status)
                viewModel.getImagesByVehicleIDLiveData( item.internal_id).observe( activity as LifecycleOwner, Observer {
                    if( it.isNotEmpty()){
                        val image = it[0]
                        Picasso.get().load(image.path ?: image.id_drive).into( binding.imgIvImage)
                    }
                    else
                        Picasso.get().load( R.mipmap.vehicle).into( binding.imgIvImage)
                })
            }
        })
    }

    override fun getItemCount(): Int = list.size

    fun setList( aux: List<Long>)
    {
        list = aux
    }
}