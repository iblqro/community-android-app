package mx.ibl.ientry.community.appinfo.firstaccess.user.type

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import mx.ibl.ientry.community.appinfo.firstaccess.user.User

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface UserTypeDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( userTypes: List<UserType>): List<Long>

    @Query( """
        SELECT * FROM user_type WHERE is_owner = 1
    """)
    fun getUserTypes(): List<UserType>

    @Query( "DELETE FROM user_type")
    fun deleteAll()
}