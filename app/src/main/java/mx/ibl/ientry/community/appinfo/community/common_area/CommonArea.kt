package mx.ibl.ientry.community.appinfo.community.common_area

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "common_area")
data class CommonArea(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val id: Long = 0,
    val status_id: Long,
    val community_id: Long,
    val name: String,
    val description: String,
    val price: Double = 0.0,
    val created_at: Date = Common.date(),
    val updated_at: Date
)