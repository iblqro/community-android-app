package mx.ibl.ientry.community.appinfo.firstaccess.day

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "day")
data class Day(
    @PrimaryKey
    val id: Long,
    val name: String
)