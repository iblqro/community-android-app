package mx.ibl.ientry.community.appinfo.community.residence

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "residence")
data class Residence(
    @PrimaryKey
    val id: Long,
    val status_id: Long,
    val community_id: Long,
    val user_id: Long? = null,
    val internal_user_id: Long? = null,
    val name: String? = null,
    val address: String? = null,
    val house_deed: String? = null,
    val exterior_number: Int,
    val interior_number: Int? = null,
    val latitude: Double? = null,
    val longitude: Double? = null,
    val created_at: Date = Common.date(),
    val updated_at: Date? = null
)