package mx.ibl.ientry.community.ui.list.residence

import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemResidenceBinding

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ResidenceVH(val view: ItemResidenceBinding): RecyclerView.ViewHolder( view.root)