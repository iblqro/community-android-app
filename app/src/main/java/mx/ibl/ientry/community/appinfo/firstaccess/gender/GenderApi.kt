package mx.ibl.ientry.community.appinfo.firstaccess.gender

import retrofit2.http.GET

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface GenderApi {

    @GET( "gender/v1/all")
    suspend fun getAll(): List<Gender>
}