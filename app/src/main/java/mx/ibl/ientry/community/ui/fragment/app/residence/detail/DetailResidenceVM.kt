package mx.ibl.ientry.community.ui.fragment.app.residence.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.community.residence.ResidenceItem
import mx.ibl.ientry.community.appinfo.community.residence.ResidenceRepository
import mx.ibl.ientry.community.appinfo.community.residence.image.ResidenceImageRepository
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.appinfo.firstaccess.user.type.UserType
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.VehicleRepository

class DetailResidenceVM(app: Application, val id: Long) : AndroidViewModel( app) {

    private val residenceRepository: ResidenceRepository by lazy { ResidenceRepository( app) }
    private val vehicleRepository: VehicleRepository by lazy { VehicleRepository( app) }
    private val userRepository: UserRepository by lazy { UserRepository( app) }
    private val residenceImageRepository: ResidenceImageRepository by lazy { ResidenceImageRepository( app) }

    val residence: LiveData<ResidenceItem?> = residenceRepository.getResidencyItem( id)
    val vehicles: LiveData<List<Long>> = vehicleRepository.getIDsVehicleByResidencyIDLiveData( id)
    val occupants: LiveData<List<Long>> = userRepository.getIDsUserInResidencyByIDAndCategoriesLiveData( UserType.listOfResidencyUsers, id)
    val workers: LiveData<List<Long>> = userRepository.getIDsUserInResidencyByIDAndCategoriesLiveData( UserType.listOfWorkersResidency, id)
    val images: LiveData<List<Long>> = residenceImageRepository.getIDsImagesByResidencyIDLiveData( id)



}