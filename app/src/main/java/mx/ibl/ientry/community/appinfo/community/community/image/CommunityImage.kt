package mx.ibl.ientry.community.appinfo.community.community.image

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Friday 30, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "community_image")
data class CommunityImage(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val urbanization_id: Long,
    val id_drive: String? = URL_IMAGE,
    val path: String? = URL_IMAGE, // to internal control
    val description: String?,
    val created_at: Date = Common.date()
){
    companion object
    {
        private val URL_IMAGE = "SIN_URL_IMAGEN"
    }
}