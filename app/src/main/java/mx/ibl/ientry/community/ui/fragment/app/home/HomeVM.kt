package mx.ibl.ientry.community.ui.fragment.app.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class HomeVM( private val app: Application) : AndroidViewModel( app) {

}