package mx.ibl.ientry.community.ui.fragment.app.residence.detail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class DetailResidenceVMF(private val app: Application, private val id: Long): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if( modelClass.isAssignableFrom( DetailResidenceVM::class.java))
            return DetailResidenceVM( app, id) as T

        throw IllegalArgumentException( "Unknown ViewModel DetailResidency class")
    }
}