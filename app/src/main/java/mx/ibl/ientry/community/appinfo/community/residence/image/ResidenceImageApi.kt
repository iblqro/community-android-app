package mx.ibl.ientry.community.appinfo.community.residence.image

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface ResidenceImageApi {

    // val file =  File( event.path_evidencia!!)
    // val requestFile = file.asRequestBody("image/*".toMediaTypeOrNull())
    // val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
    // val id = event.id_evidencia.toString().toRequestBody(MultipartBody.FORM)


    @GET( "residency-image/v1/{id}")
    fun get( @Path( "id") id: Long): Call<List<ResidenceImage>>

    @Multipart
    @POST( "residency-image/v1/create")
    fun create(
        @Part( "file") image: MultipartBody.Part,
        @Part( "residency-image") residenceImage: ResidenceImage
    ): Call<ResidenceImage>
}