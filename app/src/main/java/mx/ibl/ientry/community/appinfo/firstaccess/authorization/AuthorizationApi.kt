package mx.ibl.ientry.community.appinfo.firstaccess.authorization

import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface AuthorizationApi {

    @GET( "authorization/v1/{id}")
    fun getToAuthorization( @Path( "id") id: Long): Call<List<Authorization>>

    @POST( "authorization/v1/create")
    fun create( @Body body: Authorization): Call<Authorization>

    @PUT( "authorization/v1/update")
    fun update( @Body body: Authorization): Call<Authorization>
}