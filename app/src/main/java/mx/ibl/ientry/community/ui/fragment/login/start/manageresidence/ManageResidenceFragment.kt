package mx.ibl.ientry.community.ui.fragment.login.start.manageresidence

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentManageResidenceBinding
import mx.ibl.ientry.community.ui.activity.login.LogInVM
import mx.ibl.ientry.community.ui.activity.main.MainActivity
import mx.ibl.ientry.community.util.Common

class ManageResidenceFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentManageResidenceBinding
    private lateinit var viewModelActivity: LogInVM

    companion object{
        private const val TAG = "ManageResidenceFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( LogInVM::class.java)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_manage_residence, container, false)
        requireActivity().onBackPressedDispatcher.addCallback( this){
            Common.showSnackBar( binding.root, getString( R.string.lb_warning_forced_section), requireContext())
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fmrItemMessage.btnImYes.setOnClickListener( this)
        binding.fmrItemMessage.btnImNo.setOnClickListener( this)
        binding.fmrItemMessage.btnImNo.text =  getString( R.string.lb_dont_live_there)
        binding.fmrItemMessage.lbImSuggestion.visibility = View.GONE

        lifecycleScope.launch( Dispatchers.Default){
            whenCreated {
                viewModelActivity.residence.observe( viewLifecycleOwner, {
                    it?.let {
                        binding.fmrItemMessage.lbImMessage.text = String.format( getString( R.string.lb_assign_residence), it.exterior_number, it.community_id)
                    }
                })
                viewModelActivity.residences.observe( viewLifecycleOwner, {})
            }
        }
    }

    private fun enterToApplication()
    {
        val intent = Intent( requireActivity(), MainActivity::class.java)
        requireActivity().startActivity( intent)
        requireActivity().overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_left)
        requireActivity().finish()
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id){
                // Sí vivirá ahí //
                binding.fmrItemMessage.btnImYes.id -> {
                    // Aún tiene casas por asignar
                    if( viewModelActivity.residences.value!!.size - 1 > 0)
                        lifecycleScope.launch( Dispatchers.Default){
                            viewModelActivity.setUserToLiveThere()
                            withContext( Dispatchers.Main){
                                Common.showSnackBar( binding.root, getString( R.string.lb_other_residence), requireContext())
                            }
                        }
                    // Ya no tiene casas por asignar //
                    else
                        enterToApplication()
                }
                // No vivirá ahí //
                binding.fmrItemMessage.btnImNo.id -> {
                   findNavController().navigate(R.id.action_manageResidenceFragment_to_assignAdministratorFragment)
                }
                else -> Log.e( TAG, "ID: ${it.id} Unknown")
            }
        }
    }
}