package mx.ibl.ientry.community.appinfo.firstaccess.working_hour

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface WorkingHourDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( workingHours: List<WorkingHour>): List<Long>

    @Insert
    fun insert( workingHour: WorkingHour): Long

    @Update
    fun update( workingHour: WorkingHour)

    @Query( "DELETE FROM working_hour")
    fun deleteAll()

    @Query( """
        SELECT * FROM working_hour WHERE id_user = :id ORDER BY id_day DESC
    """)
    fun getWorkingHourUser( id: Long): LiveData<List<WorkingHour>>

    @Query( """
        SELECT * FROM working_hour WHERE id_vehicle = :id ORDER BY id_day DESC
    """)
    fun getWorkingHourVehicle( id: Long): LiveData<List<WorkingHour>>
}