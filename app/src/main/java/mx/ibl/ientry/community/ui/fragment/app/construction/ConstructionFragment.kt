package mx.ibl.ientry.community.ui.fragment.app.construction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentConstructionBinding
import mx.ibl.ientry.community.ui.activity.main.MainVM
import mx.ibl.ientry.community.util.emun.Option

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ConstructionFragment : Fragment() {

    private lateinit var viewModel: ConstructionVM
    private lateinit var binding: FragmentConstructionBinding
    private lateinit var viewModelActivity: MainVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( MainVM::class.java)
        viewModel = ViewModelProvider(this).get(ConstructionVM::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_construction, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModelActivity.setOption( Option.CONSTRUCTION)
    }
}