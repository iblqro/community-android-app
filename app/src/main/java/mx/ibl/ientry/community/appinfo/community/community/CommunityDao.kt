package mx.ibl.ientry.community.appinfo.community.community

import androidx.lifecycle.LiveData
import androidx.room.*
import mx.ibl.ientry.community.appinfo.community.community.approveversion.Vote
import mx.ibl.ientry.community.util.emun.Status

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface CommunityDao {

    // Community SQL //
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(communities: List<Community>): List<Long>

    @Insert
    fun insert(community: Community): Long

    @Update
    fun update(community: Community)

    @Query("DELETE FROM community")
    fun deleteAll()

    @Query("SELECT * FROM community WHERE id = :id")
    fun getByIDCommunity(id: Long): LiveData<Community?>

    @Query("SELECT id FROM community")
    fun getAllCommunities(): LiveData<List<Long>>

    @Query("SELECT * FROM community WHERE community_id = :id")
    fun getUrbanizationChildren(id: Long): LiveData<List<Community>>

    @Query("""
        SELECT c1.id AS id, c1.name AS name, c1.address AS address, c1.status_id AS status_id,
        c2.id AS community_id, c2.name AS father_name
        FROM community c1
        LEFT JOIN community c2 ON c1.community_id = c2.id
        WHERE c1.id = :id
    """
    )
    fun getCommunityDetailByCommunityID(id: Long): LiveData<CommunityDetail?>

    // Approve Community Version SQL //
    @Query( """
        SELECT u.name AS title, wt.name AS subtitle, acv.is_approve AS value FROM user u
        INNER JOIN worker_community wc ON wc.user_id = u.id AND wc.status_id = :status
        INNER JOIN work_type wt ON wt.id =  wc.work_type_id
        INNER JOIN community_version cv ON cv.community_id = wc.community_id AND cv.is_approve IS NULL
        LEFT JOIN approve_community_version acv ON acv.user_id = wc.user_id
        WHERE wc.community_id = :id;
    """
    )
    fun getApproveCommunityVersionByCommunityID( id: Long, status: Long = Status.STATUS_40.key): LiveData<List<Vote>>
}