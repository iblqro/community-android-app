package mx.ibl.ientry.community.appinfo.community.residence

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.network.ServiceGenerator

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ResidenceRepository(context: Context) {

    private val userRepository: UserRepository by lazy { UserRepository( context) }
    private val dao: ResidenceDao by lazy { AppDatabase.getInstance( context).residenceDao() }
    private val api: ResidenceApi by lazy { ServiceGenerator.createService( context, ResidenceApi::class.java, userRepository.token) }

    fun getAllIDsLiveData(): LiveData<List<Long>> = dao.gelAllIDsLiveData( userRepository.user.internal_id)
    fun getResidencyItem( id: Long): LiveData<ResidenceItem?> = dao.getResidenceItemLiveData( id)

    fun getResidencesToAssign(): List<Residence> = listOf(
        Residence(
            id = 1, status_id = 1, community_id = 1,  exterior_number = 101
        ),
        Residence(
            id = 2, status_id = 1, community_id = 1,  exterior_number = 32
        ),
        Residence(
            id = 3, status_id = 1, community_id = 1,  exterior_number = 66
        )
    )

    fun getResidenceToAssign(): Residence? = Residence(
        id = 3, status_id = 1, community_id = 1, exterior_number = 66
    )
}