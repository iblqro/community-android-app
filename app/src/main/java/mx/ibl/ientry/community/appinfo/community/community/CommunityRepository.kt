package mx.ibl.ientry.community.appinfo.community.community

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.appinfo.community.community.approveversion.Vote
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.network.ServiceGenerator

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommunityRepository( val app: Context) {

    private val userRepository: UserRepository by lazy { UserRepository( app) }
    private val dao: CommunityDao by lazy { AppDatabase.getInstance( app).communityDao() }
    private val api: CommunityApi by lazy { ServiceGenerator.createService( app, CommunityApi::class.java, userRepository.token) }

    // Community request //
    fun getAllCommunities(): LiveData<List<Long>> =  dao.getAllCommunities()
    fun getByIDCommunity(id: Long): LiveData<Community?> = dao.getByIDCommunity( id)
    fun getCommunityDetailByCommunityID(id: Long): LiveData<CommunityDetail?> = dao.getCommunityDetailByCommunityID( id)

    // Approve Community Version request //
    fun getApproveCommunityVersionByCommunityID( id: Long): LiveData<List<Vote>> = dao.getApproveCommunityVersionByCommunityID( id)
}