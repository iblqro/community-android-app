package mx.ibl.ientry.community.appinfo.firstaccess.user

/**
 * @author ISC Luis Cornejo
 * @since Friday 30, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
data class UserLogin(
    var username: String = "",
    var password: String = "",
)