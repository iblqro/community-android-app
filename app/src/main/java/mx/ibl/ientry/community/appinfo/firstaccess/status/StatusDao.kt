package mx.ibl.ientry.community.appinfo.firstaccess.status

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface StatusDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( status: List<Status>): List<Long>

    @Query( "DELETE FROM status")
    fun deleteAll()

    @Query( """
        SELECT name FROM status WHERE id = :id
    """)
    fun getStatusNameByID( id: Long): String

    @Query( """
        SELECT id FROM status WHERE name = :name
    """)
    fun getIDStatusByName( name: String): Long

    @Query( """
        SELECT * FROM status
    """)
    fun getAllLiveData(): LiveData<List<Status>>
}