package mx.ibl.ientry.community.ui.fragment.login.start.password

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository

/**
 * @author ISC Luis Cornejo
 * @since Tuesday 22, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class PasswordVM( app: Application): AndroidViewModel( app) {

    private val userRepository: UserRepository by lazy { UserRepository( app) }

    var lastPassword = ""
    val password = MutableLiveData<String>().apply {
        value = ""
    }

    fun isOkToChangePassword(): Boolean = password.value!!.isNotEmpty() && password.value!! != lastPassword && lastPassword.isNotEmpty()

    fun updatePassword(){

    }
}