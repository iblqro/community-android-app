package mx.ibl.ientry.community.ui.list.community

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.ItemCommunityBinding
import mx.ibl.ientry.community.ui.fragment.app.community.CommunityVM
import mx.ibl.ientry.community.ui.fragment.app.community.detail.CommunityDetailFragment
import mx.ibl.ientry.community.ui.list.vote.VoteAdapter
import mx.ibl.ientry.community.util.Common
import mx.ibl.ientry.community.util.emun.Status

/**
 * @author ISC Luis Cornejo
 * @since Thursday 24, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommunityAdapter(val activity: ViewModelStoreOwner) : RecyclerView.Adapter<CommunityVH>() {

    private val viewModel = ViewModelProvider(activity).get(CommunityVM::class.java)
    private lateinit var context: Context
    private lateinit var navController: NavController
    private lateinit var view: View
    private var list = listOf<Long>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommunityVH {
        this.context = parent.context
        this.navController = parent.findNavController()
        this.view = parent.rootView

        return CommunityVH(
            ItemCommunityBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CommunityVH, position: Int) {
        val id = list[position]
        val view = holder.view
        viewModel.getUrbanizationDetailByCommunityID(id).observe(activity as LifecycleOwner) {
            it?.let { item ->
                view.lbIcGoToDetails.setOnClickListener {
                    when (item.status_id) {
                        Status.STATUS_20.key -> {
                        } //ToDo: Go To Approve //
                        Status.STATUS_21.key -> {
                        }  //ToDo: Go To Update Community Setting //
                        Status.STATUS_22.key -> {
                            val bundle = bundleOf(CommunityDetailFragment.COMMUNITY_ID to id)
                            navController.navigate(
                                R.id.action_nav_community_to_communityDetailFragment,
                                bundle
                            )
                        }
                        Status.STATUS_23.key, Status.STATUS_24.key ->
                            Common.showSnackBar(
                                this.view,
                                context.getString(R.string.lb_message_community_status_23_24),
                                this.context
                            )
                    }
                }
                view.lbIcCommunity.text = it.name // Community Name //
                view.lbIcAddress.text = it.address // Community Address //
                when (item.status_id) {
                    Status.STATUS_20.key, Status.STATUS_21.key -> {
                        view.lbIcStatus.setTextColor(context.getColor(if (item.status_id == Status.STATUS_20.key) R.color.blue else R.color.yellow))
                        view.lbIcStatus.text =
                            context.getString(if (item.status_id == Status.STATUS_20.key) Status.STATUS_20.value else Status.STATUS_21.value)

                        val adapter = VoteAdapter()
                        val manager = Common.getHorizontalManager(this.context)
                        val controller =
                            AnimationUtils.loadLayoutAnimation(this.context, R.anim.lista_fall_down)

                        view.rvIcVotes.adapter = adapter
                        view.rvIcVotes.layoutManager = manager
                        view.rvIcVotes.layoutAnimation = controller

                        viewModel.getApproveCommunityVersionByCommunityID(item.id)
                            .observe(activity as LifecycleOwner) {
                                adapter.setList(it)
                                adapter.notifyDataSetChanged()
                                view.rvIcVotes.scheduleLayoutAnimation()
                            }

                        view.cvIrContainer.setOnClickListener {
                            view.llIcContainerVotes.visibility =
                                if (view.llIcContainerVotes.visibility == View.GONE) View.VISIBLE else View.GONE
                        }
                    }
                    Status.STATUS_22.key -> {
                        view.lbIcStatus.setTextColor(context.getColor(R.color.green))
                        view.lbIcStatus.text = context.getString(Status.STATUS_22.value)
                    }
                    Status.STATUS_23.key, Status.STATUS_24.key -> {
                        view.lbIcStatus.setTextColor(context.getColor(R.color.red))
                        view.lbIcStatus.text =
                            if (item.status_id == Status.STATUS_23.key) context.getString(Status.STATUS_23.value)
                            else context.getString(Status.STATUS_24.value)
                    }
                }
                view.llIcContainerFather.visibility =
                    if (item.community_id == null) View.GONE else {
                        view.lbIcFather.text = it.father_name // Community Father Name //
                        View.VISIBLE
                    }

            }
        }
    }

    override fun getItemCount(): Int = list.size

    fun setList(list: List<Long>) {
        this.list = list
    }
}