package mx.ibl.ientry.community.ui.activity.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.ActivityMainBinding
import mx.ibl.ientry.community.util.emun.Option

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController
    private lateinit var viewModel: MainVM
    private val startForResult = registerForActivityResult( ActivityResultContracts.StartActivityForResult()) {}


    companion object{
        private const val TAG: String = "MainActivity"
        private const val REQUEST_CODE_PERMISSIONS = 101
        private val REQUIRED_PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView( this, R.layout.activity_main)
        viewModel = ViewModelProvider( this).get( MainVM::class.java)
        val hostFragment = ( supportFragmentManager.findFragmentById( binding.contentMain.navHostFragment.id) as NavHostFragment)
        navController = hostFragment.navController

        requestPermissions()

        setSupportActionBar( binding.contentMain.bottomAppBarMain)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_community,
                R.id.nav_residence,
                R.id.nav_construction,
                R.id.nav_penalty,
                R.id.nav_payment,
                R.id.nav_profile,
            ), binding.drawerLayout)

        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)

        binding.contentMain.fabMain.setOnClickListener( this)

        lifecycleScope.launch( Dispatchers.Default){
            whenCreated {
                val name = viewModel.user.name
                binding.navView.getHeaderView(0).findViewById<TextView>( R.id.lb_nhm_username).text = name
                binding.navView.getHeaderView(0).findViewById<TextView>( R.id.lb_nhm_email).text = viewModel.user.email

                viewModel.option.observe( this@MainActivity, { option ->
                    binding.contentMain.fabMain.hide()
                    when( option) {
                        Option.HOME -> {
                            binding.contentMain.fabMain.setImageDrawable(
                                ContextCompat.getDrawable(
                                    this@MainActivity,
                                    R.drawable.ic_home
                                ))

                            binding.contentMain.fabMain.show()
                            binding.contentMain.bottomAppBarMain.visibility = View.VISIBLE
                        }
                        Option.CONSTRUCTION -> {
                            binding.contentMain.fabMain.setImageDrawable(
                                ContextCompat.getDrawable(
                                    this@MainActivity,
                                    R.drawable.ic_construction
                                ))

                            binding.contentMain.fabMain.show()
                            binding.contentMain.bottomAppBarMain.visibility = View.VISIBLE
                        }
                        Option.PENALTY -> {
                            binding.contentMain.fabMain.setImageDrawable(
                                ContextCompat.getDrawable(
                                    this@MainActivity,
                                    R.drawable.ic_penalty
                                ))

                            binding.contentMain.fabMain.show()
                            binding.contentMain.bottomAppBarMain.visibility = View.VISIBLE
                        }
                        Option.PAYMENT -> {
                            binding.contentMain.fabMain.setImageDrawable(
                                ContextCompat.getDrawable(
                                    this@MainActivity,
                                    R.drawable.ic_payment
                                ))

                            binding.contentMain.fabMain.show()
                            binding.contentMain.bottomAppBarMain.visibility = View.VISIBLE
                        }
                        Option.PROFILE, Option.NONE -> binding.contentMain.bottomAppBarMain.visibility = View.GONE
                        Option.ONLY_BAR -> binding.contentMain.bottomAppBarMain.visibility = View.VISIBLE
                    }
                })
            }
        }
    }

    private fun requestPermissions() = ActivityCompat.requestPermissions(
        this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)

    private fun dialogSettings()
    {
        AlertDialog.Builder( this, R.style.AlertDialogTheme).apply {
            setTitle( "Permissions Required")
            setMessage( "Permissions are required to app")
            setPositiveButton( R.string.lb_btn_go_to){ _, _ ->
                openSettings()
            }
        }
            .create()
            .show()
    }

    private fun openSettings()
    {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this.packageName, null)
        intent.data = uri
        startForResult.launch(intent)
    }

    private fun allPermissionsGranted(): Boolean = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission( this.baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        if( binding.drawerLayout.isDrawerOpen( GravityCompat.START))
            binding.drawerLayout.closeDrawer( GravityCompat.START)
        else
            super.onBackPressed()
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id)
            {
                binding.contentMain.fabMain.id ->
                {
                    viewModel.option.value?.let { option ->
                        when( option)
                        {
                            Option.HOME -> Toast.makeText( this, "Click in home", Toast.LENGTH_LONG).show()
                            Option.CONSTRUCTION -> Toast.makeText( this, "Click in construction", Toast.LENGTH_LONG).show()
                            Option.PENALTY -> Toast.makeText( this, "Click in penalty", Toast.LENGTH_LONG).show()
                            Option.PAYMENT -> Toast.makeText( this, "Click in payment", Toast.LENGTH_LONG).show()
                            else -> {}
                        }
                    }
                }
                else -> { Log.e( TAG, "Click on view was not found")}
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if( requestCode ==  REQUEST_CODE_PERMISSIONS)
            if( !allPermissionsGranted())
                dialogSettings()
    }
}