package mx.ibl.ientry.community.appinfo.community.catalog.user_community

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author ISC Luis Cornejo
 * @since Wednesday 05, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "user_community")
data class UserCommunity(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val id: Long = 0,
    val internal_user_id: Long = 0,
    val user_id: Long = 0,
    val community_id: Long,
    val user_type_id: Long,
)