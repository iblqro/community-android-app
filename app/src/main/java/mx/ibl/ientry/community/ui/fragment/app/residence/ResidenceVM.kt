package mx.ibl.ientry.community.ui.fragment.app.residence

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.community.residence.ResidenceRepository
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ResidenceVM(private val app: Application) : AndroidViewModel( app) {

    private val residenceRepository: ResidenceRepository by lazy { ResidenceRepository( app) }
    private val userRepository: UserRepository by lazy { UserRepository( app) }

    val listResidencies: LiveData<List<Long>> = residenceRepository.getAllIDsLiveData()
}