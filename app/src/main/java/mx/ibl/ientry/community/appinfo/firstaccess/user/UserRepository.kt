package mx.ibl.ientry.community.appinfo.firstaccess.user

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.util.Common

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class UserRepository(context: Context) {

    private val dao: UserDao by lazy { AppDatabase.getInstance(context).userDao() }

    val token: String = ""
    val user: User = User(
        status_id = 1,
        gender_id = 1,
        name = "Luis",
        password = "asdadasd",
        email = "luis.cornejo.96@hotmail.com",
        phone_number = "4427065909",
        dob = Common.date(),
        curp = "COAL970408HMCRSS00",
        is_account_logged = "asdasdasdsd",
        created_at = Common.date(),
        updated_at = Common.date(),
        is_main = true
    )


    fun getUserLiveData(id: Long): LiveData<User?> = dao.getUserLiveData(id)
    fun getUserLiveDataSafeNull(id: Long): LiveData<User> = dao.getUserLiveDataSafeNull(id)
    fun getIDsUserInResidencyByIDAndCategoriesLiveData(
        types: List<Long>,
        id: Long
    ): LiveData<List<Long>> = dao.getIDsUserInResidencyByIDAndCategoriesLiveData(types, id)
    fun insert( user: User): Long =  dao.insert( user)
    fun update( user: User) =  dao.update( user)
}