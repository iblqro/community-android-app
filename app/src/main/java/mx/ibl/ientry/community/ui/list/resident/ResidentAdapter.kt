package mx.ibl.ientry.community.ui.list.resident

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemResidentBinding

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ResidentAdapter(private val activity: ViewModelStoreOwner): RecyclerView.Adapter<ResidentVH>() {

    private lateinit var context: Context
    private var list = listOf<Long>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResidentVH {
        context = parent.context

        return ResidentVH( ItemResidentBinding.inflate( LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: ResidentVH, position: Int) {
        val binding = holder.view
        val id = list[position]
    }

    override fun getItemCount(): Int = list.size

    fun setList( aux: List<Long>)
    {
        list = aux
    }
}