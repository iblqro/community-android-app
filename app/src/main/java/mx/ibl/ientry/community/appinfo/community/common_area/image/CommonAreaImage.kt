package mx.ibl.ientry.community.appinfo.community.common_area.image

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "common_area_image")
data class CommonAreaImage(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long,
    val id_common_area: Long,
    val id_drive: String? = URL_IMAGE,
    val path: String? = URL_IMAGE,
    val description: String?,
    val created_at: Date = Common.date()
){
    companion object
    {
        private val URL_IMAGE = "SIN_URL_IMAGEN"
    }
}