package mx.ibl.ientry.community.ui.fragment.login.forget.resetpassword

import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentResetPasswordBinding
import mx.ibl.ientry.community.ui.activity.login.ForgetPasswordVM
import mx.ibl.ientry.community.ui.fragment.login.forget.method.SelectMethodFragment
import mx.ibl.ientry.community.ui.fragment.login.start.code.CodeFragment
import mx.ibl.ientry.community.util.Common
import mx.ibl.ientry.community.util.sms.ReceiveSms

class ResetPasswordFragment : Fragment(), View.OnClickListener, TextWatcher, View.OnFocusChangeListener {

    private lateinit var binding: FragmentResetPasswordBinding
    private lateinit var viewModelActivity: ForgetPasswordVM
    private lateinit var option: String
    private lateinit var receiveSms: ReceiveSms

    companion object{
        private const val TAG = "ResetPasswordFragment"
        const val IS_SMS = "is_sms"
        const val IS_EMAIL = "is_email"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            option = it.getString( SelectMethodFragment.RESET_PASSWORD_OPTION, "")
        }
        receiveSms = ReceiveSms()
        viewModelActivity = ViewModelProvider( requireActivity()).get( ForgetPasswordVM::class.java)
        viewModelActivity.option = option
        viewModelActivity.setTime(60)
        viewModelActivity.setMessage( "")
        requireActivity().registerReceiver( receiveSms, IntentFilter(CodeFragment.SMS_ACTION))

        requireActivity().onBackPressedDispatcher.addCallback(this){
            findNavController().popBackStack( R.id.forgetPasswordFragment, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_reset_password, container, false)

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().unregisterReceiver( receiveSms)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.imgFrpBack.setOnClickListener(this)
        binding.frpResetPassword.lbIrpNotIncoming.setOnClickListener( this)
        setEnableComponents()
        // Logic para SMS //
        if( viewModelActivity.option != null && viewModelActivity.option == IS_SMS)
        {
            viewModelActivity.timeMessage.observe(viewLifecycleOwner, {
                lifecycleScope.launch(Dispatchers.Default){
                    withContext( Dispatchers.Main){
                        if ( it > 0)
                        {
                            binding.frpResetPassword.lbIrpTime.text = String.format( " %s s.", it )
                            binding.frpResetPassword.lbIrpTimeMessage.visibility = View.VISIBLE
                        }
                        else {
                            binding.frpResetPassword.lbIrpTime.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.red
                                )
                            )
                            binding.frpResetPassword.lbIrpTimeMessage.visibility = View.GONE
                            binding.frpResetPassword.lbIrpTime.text = getString(R.string.lb_fcp_time_out)
                        }
                    }
                    delay(1000)
                    withContext( Dispatchers.Main){
                        if( it > 0 ) viewModelActivity.setTime( it - 1)
                    }
                }

            })
            viewModelActivity.message.observe(viewLifecycleOwner, {
                if (it.isNotEmpty()) {
                    lifecycleScope.launch(Dispatchers.Default) {
                        delay(100)
                        withContext(Dispatchers.Main){
                            binding.frpResetPassword.txtIrpNumber1.setText(it[0].toString())
                        }
                        delay(100)
                        withContext(Dispatchers.Main){
                            binding.frpResetPassword.txtIrpNumber2.setText(it[1].toString())
                        }
                        delay(100)
                        withContext(Dispatchers.Main){
                            binding.frpResetPassword.txtIrpNumber3.setText(it[2].toString())
                        }
                        delay(100)
                        withContext(Dispatchers.Main){
                            binding.frpResetPassword.txtIrpNumber4.setText(it[3].toString())
                        }
                        delay(100)
                        withContext(Dispatchers.Main){
                            binding.frpResetPassword.txtIrpNumber5.setText(it[4].toString())
                        }
                        delay(100)
                        withContext(Dispatchers.Main){
                            binding.frpResetPassword.txtIrpNumber6.setText(it[5].toString())
                        }
                        delay(350)
                        withContext(Dispatchers.Main){
                            findNavController().navigate( R.id.action_resetPasswordFragment_to_changePasswordFragment)
                        }
                    }
                }
                else
                {
                    binding.frpResetPassword.txtIrpNumber1.setText( "")
                    binding.frpResetPassword.txtIrpNumber2.setText( "")
                    binding.frpResetPassword.txtIrpNumber3.setText( "")
                    binding.frpResetPassword.txtIrpNumber4.setText( "")
                    binding.frpResetPassword.txtIrpNumber5.setText( "")
                    binding.frpResetPassword.txtIrpNumber6.setText( "")
                }
            })
            receiveSms.code.observe(viewLifecycleOwner, {
                viewModelActivity.setMessage( it)
            })
            receiveSms.error.observe( viewLifecycleOwner, {
                Common.showSnackBar( binding.root, it, requireContext())
            })
        }
    }
    
    private fun setEnableComponents(){
        if( viewModelActivity.option != null && viewModelActivity.option == IS_SMS)
        {
            binding.frpResetPassword.lbIrpMessage.text = getString( R.string.lb_irp_option_sms)
            binding.frpResetPassword.lbIrpTip.visibility = View.GONE

            binding.frpResetPassword.ctxtIrpNumber1.isEnabled = false
            binding.frpResetPassword.ctxtIrpNumber2.isEnabled = false
            binding.frpResetPassword.ctxtIrpNumber3.isEnabled = false
            binding.frpResetPassword.ctxtIrpNumber4.isEnabled = false
            binding.frpResetPassword.ctxtIrpNumber5.isEnabled = false
            binding.frpResetPassword.ctxtIrpNumber6.isEnabled = false
        } 
        else if( viewModelActivity.option == IS_EMAIL)
        {
            binding.frpResetPassword.lbIrpMessage.text = getString( R.string.lb_irp_option_email)
            binding.frpResetPassword.lbIrpTip.visibility = View.VISIBLE
            binding.frpResetPassword.lbIrpTime.visibility = View.GONE

            binding.frpResetPassword.ctxtIrpNumber1.isEnabled = true
            binding.frpResetPassword.ctxtIrpNumber2.isEnabled = true
            binding.frpResetPassword.ctxtIrpNumber3.isEnabled = true
            binding.frpResetPassword.ctxtIrpNumber4.isEnabled = true
            binding.frpResetPassword.ctxtIrpNumber5.isEnabled = true
            binding.frpResetPassword.ctxtIrpNumber6.isEnabled = true

            setListeners()
        }
    }

    private fun setListeners()
    {
        binding.frpResetPassword.txtIrpNumber1.addTextChangedListener( this)
        binding.frpResetPassword.txtIrpNumber2.addTextChangedListener( this)
        binding.frpResetPassword.txtIrpNumber3.addTextChangedListener( this)
        binding.frpResetPassword.txtIrpNumber4.addTextChangedListener( this)
        binding.frpResetPassword.txtIrpNumber5.addTextChangedListener( this)
        binding.frpResetPassword.txtIrpNumber6.addTextChangedListener( this)

        binding.frpResetPassword.txtIrpNumber1.onFocusChangeListener = this
        binding.frpResetPassword.txtIrpNumber2.onFocusChangeListener = this
        binding.frpResetPassword.txtIrpNumber3.onFocusChangeListener = this
        binding.frpResetPassword.txtIrpNumber4.onFocusChangeListener = this
        binding.frpResetPassword.txtIrpNumber5.onFocusChangeListener = this
        binding.frpResetPassword.txtIrpNumber6.onFocusChangeListener = this
    }

    private fun codeNotEmpty(): Boolean =
        binding.frpResetPassword.txtIrpNumber1.text.toString().isNotEmpty() &&
        binding.frpResetPassword.txtIrpNumber2.text.toString().isNotEmpty() &&
        binding.frpResetPassword.txtIrpNumber3.text.toString().isNotEmpty() &&
        binding.frpResetPassword.txtIrpNumber4.text.toString().isNotEmpty() &&
        binding.frpResetPassword.txtIrpNumber5.text.toString().isNotEmpty() &&
        binding.frpResetPassword.txtIrpNumber6.text.toString().isNotEmpty()

    private fun navigateToChangePassword()
    {
        if( codeNotEmpty())
        {
            if( viewModelActivity.checkIsTheSameCode()){
                lifecycleScope.launch( Dispatchers.Default){
                    delay(350)
                    withContext( Dispatchers.Main){
                        findNavController().navigate( R.id.action_resetPasswordFragment_to_changePasswordFragment)
                    }
                }
            }
            else
                Common.showSnackBar( binding.root, getString( R.string.lb_code_wrong), requireContext())
        }
        else
            Common.showSnackBar( binding.root, getString( R.string.lb_code_imcomplete), requireContext())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        navigateToChangePassword()
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        v?.let {
            if( codeNotEmpty())
                when( it.id)
                {
                    binding.frpResetPassword.txtIrpNumber1.id,
                    binding.frpResetPassword.txtIrpNumber2.id,
                    binding.frpResetPassword.txtIrpNumber3.id,
                    binding.frpResetPassword.txtIrpNumber4.id,
                    binding.frpResetPassword.txtIrpNumber5.id,
                    binding.frpResetPassword.txtIrpNumber6.id ->{
                        if( !hasFocus) Common.hideKeyboard( requireContext(), it)
                    }
                }
        }
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id){
                binding.frpResetPassword.lbIrpNotIncoming.id ->{
                    if( viewModelActivity.option != null && viewModelActivity.option == IS_SMS) {
                        if( viewModelActivity.timeMessage.value!! > 0)
                            Common.showSnackBar( binding.root, getString(R.string.lb_sms_not_error), requireContext())
                        else
                        {
                            binding.frpResetPassword.lbIrpTime.setTextColor(
                                ContextCompat.getColor(
                                    requireContext(),
                                    R.color.gray
                                )
                            )
                            viewModelActivity.setTime(60)
                        }
                    }
                    else{
                        viewModelActivity.reSendEmail()
                        Common.showSnackBar( binding.root, getString( R.string.lb_email_re_send), requireContext())
                    }

                }
                binding.imgFrpBack.id -> findNavController().popBackStack( R.id.log_in_nav, false)
                else -> Log.e( TAG, "ID: ${it.id} Unknown")
            }
        }
    }

}