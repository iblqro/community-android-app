package mx.ibl.ientry.community.ui.fragment.app.community

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentCommunityBinding
import mx.ibl.ientry.community.ui.activity.main.MainVM
import mx.ibl.ientry.community.ui.list.community.CommunityAdapter
import mx.ibl.ientry.community.util.Common
import mx.ibl.ientry.community.util.emun.Option

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommunityFragment : Fragment() {

    private lateinit var viewModel: CommunityVM
    private lateinit var binding: FragmentCommunityBinding
    private lateinit var viewModelActivity: MainVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( MainVM::class.java)
        viewModelActivity.setOption( Option.ONLY_BAR)

        viewModel = ViewModelProvider(this).get(CommunityVM::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_community, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter =  CommunityAdapter( this)
        val manager = Common.getVerticalManager( requireContext())
        val controller = AnimationUtils.loadLayoutAnimation( requireContext(), R.anim.lista_fall_down)

        binding.rvFcCommunities.adapter = adapter
        binding.rvFcCommunities.layoutAnimation =  controller
        binding.rvFcCommunities.layoutManager = manager
        binding.rvFcCommunities.setHasFixedSize( true)

        lifecycleScope.launch( Dispatchers.Default){
            whenCreated {
                viewModel.communities.observe( viewLifecycleOwner, {
                    if( it.isNotEmpty())
                    {
                        binding.fcItemNoData.root.visibility = View.GONE
                        binding.rvFcCommunities.visibility = View.VISIBLE

                        adapter.setList( it)
                        adapter.notifyDataSetChanged()
                        binding.rvFcCommunities.scheduleLayoutAnimation()
                    }
                    else
                    {
                        // ToDo: Quitar lo no comentado y poner lo comentado //
                        adapter.setList( listOf( 1L, 2L, 3L, 4L, 5L))
                        adapter.notifyDataSetChanged()
                        binding.rvFcCommunities.scheduleLayoutAnimation()
                        // binding.rvFrResidencies.visibility = View.GONE
                        // binding.fcItemNoData.root.visibility = View.VISIBLE
                    }
                    binding.lbFcTotal.text = String.format( getString( R.string.lb_format_total), it.size)
                })
            }
        }
    }

}