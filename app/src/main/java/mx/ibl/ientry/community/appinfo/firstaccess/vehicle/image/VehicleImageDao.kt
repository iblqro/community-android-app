package mx.ibl.ientry.community.appinfo.firstaccess.vehicle.image

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface VehicleImageDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( vehicleImages: List<VehicleImage>): List<Long>

    @Insert
    fun insert( vehicleImage: VehicleImage): Long

    @Update
    fun update( vehicleImage: VehicleImage)

    @Query( "DELETE FROM vehicle_image")
    fun deleteAll()

    @Query( """
        SELECT * FROM vehicle_image WHERE id_internal_vehicle = :id
    """)
    fun getImagesByVehicleIDLiveData( id: Long): LiveData<List<VehicleImage>>
}