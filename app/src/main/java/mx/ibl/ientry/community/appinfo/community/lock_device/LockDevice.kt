package mx.ibl.ientry.community.appinfo.community.lock_device

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "lock_device")
data class LockDevice(
    @PrimaryKey
    val id: Long,
    val id_status: Long,
    val id_urbanization: Long,
    val id_residency: Long?,
    val name: String,
    val model: String,
    val ip: String,
    val mac: String,
    val password: String?,
    val serial_number: String,
    val created_at: Date,
    val updated_at: Date?
)