package mx.ibl.ientry.community.appinfo.firstaccess.access_method

import retrofit2.http.GET

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface AccessMethodApi {

    @GET( "access-method/v1/all")
    suspend fun getAll(): List<AccessMethod>
}