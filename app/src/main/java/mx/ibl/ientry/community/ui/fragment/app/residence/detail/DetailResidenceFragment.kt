package mx.ibl.ientry.community.ui.fragment.app.residence.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentDetailResidenceBinding
import mx.ibl.ientry.community.ui.list.image.ImageAdapter
import mx.ibl.ientry.community.ui.list.resident.ResidentAdapter
import mx.ibl.ientry.community.ui.list.vehicle.VehicleAdapter
import mx.ibl.ientry.community.util.Common

class DetailResidenceFragment : Fragment() {

    private lateinit var binding: FragmentDetailResidenceBinding
    private lateinit var viewModel: DetailResidenceVM
    private lateinit var viewModelFactory: DetailResidenceVMF
    private var id: Long = 0

    companion object {
        const val ID_RESIDENCY = "id_residency"
        const val TOTAL = "Total: %s"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id = it.getLong(ID_RESIDENCY, 0)
        }
        viewModelFactory = DetailResidenceVMF(requireActivity().application, id)
        viewModel = ViewModelProvider(this, viewModelFactory).get(DetailResidenceVM::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_detail_residence, container, false)

        // Message Image //
        binding.indsFdrImage.lbIwdMessage.text = getString(R.string.lb_no_data_image)
        binding.ianbFdrImage.lbItemText.text = getString( R.string.lb_item_add_image)
        // Message Vehicle //
        binding.indsFdrVehicle.lbIwdMessage.text = getString(R.string.lb_no_data_vehicle)
        binding.ianbFdrVehicle.lbItemText.text = getString( R.string.lb_item_add_vehicle)
        // Message Occupant //
        binding.indsFdrOccupant.lbIwdMessage.text = getString(R.string.lb_no_data_occupant)
        binding.ianbFdrOccupant.lbItemText.text = getString( R.string.lb_item_add_occupant)
        // Message Worker //
        binding.indsFdrWorker.lbIwdMessage.text = getString(R.string.lb_no_data_worker)
        binding.ianbFdrWorker.lbItemText.text = getString( R.string.lb_item_add_worker)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // -------- Adapters -------- //
        // Image - Residency //
        val imageAdapter = ImageAdapter( this, ImageAdapter.IMAGE_RESIDENCY)
        val imageManager = Common.getHorizontalManager( requireContext())
        val imageController = AnimationUtils.loadLayoutAnimation( requireContext(), R.anim.lista_fall_down)

        binding.rvFdrImage.adapter = imageAdapter
        binding.rvFdrImage.layoutManager = imageManager
        binding.rvFdrImage.layoutAnimation = imageController
        binding.rvFdrImage.setHasFixedSize( true)

        // Occupant //
        val occupantAdapter = ResidentAdapter( this)
        val occupantManager = Common.getHorizontalManager( requireContext())
        val occupantController = AnimationUtils.loadLayoutAnimation( requireContext(), R.anim.lista_fall_down)

        binding.rvFdrOccupant.adapter = occupantAdapter
        binding.rvFdrOccupant.layoutManager = occupantManager
        binding.rvFdrOccupant.layoutAnimation = occupantController
        binding.rvFdrOccupant.setHasFixedSize( true)

        // Vehicle //
        val vehicleAdapter = VehicleAdapter( this)
        val vehicleManager = Common.getHorizontalManager( requireContext())
        val vehicleController = AnimationUtils.loadLayoutAnimation( requireContext(), R.anim.lista_fall_down)

        binding.rvFdrVehicle.adapter = vehicleAdapter
        binding.rvFdrVehicle.layoutManager = vehicleManager
        binding.rvFdrVehicle.layoutAnimation = vehicleController
        binding.rvFdrVehicle.setHasFixedSize( true)

        // Worker //
        val workerAdapter = ResidentAdapter( this)
        val workerManager = Common.getHorizontalManager( requireContext())
        val workerController = AnimationUtils.loadLayoutAnimation( requireContext(), R.anim.lista_fall_down)

        binding.rvFdrWorker.adapter = workerAdapter
        binding.rvFdrWorker.layoutManager = workerManager
        binding.rvFdrWorker.layoutAnimation = workerController
        binding.rvFdrWorker.setHasFixedSize( true)

        lifecycleScope.launch(Dispatchers.Default) {
            whenCreated {
                // Information //
                viewModel.residence.observe(viewLifecycleOwner, {
                    it?.let { residency ->
                        binding.lbFdrResidencyName.text = residency.residence
                        binding.lbFdrNoHouse.text = if( residency.interior_number != null) "No. Exterior: ${residency.exterior_number}, Interior: ${residency.interior_number}" else "No. Exterior: ${residency.exterior_number}"
                        binding.lbFdrUrbanization.text = residency.community
                        binding.lbFdrStatus.text = residency.status
                    }
                })
                // Images //
                viewModel.images.observe( viewLifecycleOwner, {
                    if (it.isNotEmpty()) {
                        binding.indsFdrImage.root.visibility = View.GONE
                        binding.rvFdrImage.visibility = View.VISIBLE

                        imageAdapter.setList( it)
                        imageAdapter.notifyDataSetChanged()
                        binding.rvFdrImage.scheduleLayoutAnimation()
                    } else {

                        imageAdapter.setList( listOf( 0L, 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L))
                        imageAdapter.notifyDataSetChanged()
                        binding.rvFdrImage.scheduleLayoutAnimation()
                        // binding.rvFdrImage.visibility = View.GONE
                        // binding.indsFdrImage.root.visibility = View.VISIBLE
                    }
                    binding.lbFdrTitleImage.text = if( it.size == 1) getString( R.string.lb_fdr_image_title) else getString( R.string.lb_fdr_images_title)
                    binding.lbFdrNoImage.text = String.format( TOTAL, it.size)
                })
                // Occupants //
                viewModel.occupants.observe( viewLifecycleOwner,  {
                    if (it.isNotEmpty()) {
                        binding.indsFdrOccupant.root.visibility = View.GONE
                        binding.rvFdrOccupant.visibility = View.VISIBLE

                        occupantAdapter.setList( it)
                        occupantAdapter.notifyDataSetChanged()
                        binding.rvFdrOccupant.scheduleLayoutAnimation()
                    } else {

                        occupantAdapter.setList( listOf( 0L, 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L))
                        occupantAdapter.notifyDataSetChanged()
                        binding.rvFdrOccupant.scheduleLayoutAnimation()
                        // binding.rvFdrOccupant.visibility = View.GONE
                        // binding.indsFdrOccupant.root.visibility = View.VISIBLE
                    }
                    binding.lbFdrTitleOccupant.text = if( it.size == 1) getString( R.string.lb_fdr_occupant_title) else getString( R.string.lb_fdr_occupants_title)
                    binding.lbFdrNoOccupant.text = String.format( TOTAL, it.size)
                })
                // Vehicles //
                viewModel.vehicles.observe(viewLifecycleOwner,  {
                    if (it.isNotEmpty()) {
                        binding.indsFdrVehicle.root.visibility = View.GONE
                        binding.rvFdrVehicle.visibility = View.VISIBLE

                        vehicleAdapter.setList( it)
                        vehicleAdapter.notifyDataSetChanged()
                        binding.rvFdrVehicle.scheduleLayoutAnimation()
                    } else {
                        // binding.rvFdrVehicle.visibility = View.GONE
                        // binding.indsFdrVehicle.root.visibility = View.VISIBLE

                        vehicleAdapter.setList( listOf( 1L, 2L, 3L, 4L, 5L, 6L, 7L))
                        vehicleAdapter.notifyDataSetChanged()
                        binding.rvFdrVehicle.scheduleLayoutAnimation()
                    }
                    binding.lbFdrTitleVehicle.text = if( it.size == 1) getString( R.string.lb_fdr_vehicle_title) else getString( R.string.lb_fdr_vehicles_title)
                    binding.lbFdrNoVehicle.text = String.format( TOTAL, it.size)
                })
                // Worker //
                viewModel.workers.observe( viewLifecycleOwner,  {
                    if (it.isNotEmpty()) {
                        binding.indsFdrWorker.root.visibility = View.GONE
                        binding.rvFdrWorker.visibility = View.VISIBLE

                        workerAdapter.setList( it)
                        workerAdapter.notifyDataSetChanged()
                        binding.rvFdrWorker.scheduleLayoutAnimation()
                    } else {
                        // binding.rvFdrWorker.visibility = View.GONE
                        // binding.indsFdrWorker.root.visibility = View.VISIBLE

                        workerAdapter.setList( listOf( 1L, 2L, 3L, 4L, 5L, 6L, 7L))
                        workerAdapter.notifyDataSetChanged()
                        binding.rvFdrWorker.scheduleLayoutAnimation()
                    }
                    binding.lbFdrTitleWorker.text = if( it.size == 1) getString( R.string.lb_fdr_work_title) else getString( R.string.lb_fdr_workers_title)
                    binding.lbFdrNoWorker.text = String.format( TOTAL, it.size)
                })
            }
        }
    }

}