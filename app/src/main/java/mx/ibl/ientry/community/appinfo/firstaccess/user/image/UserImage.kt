package mx.ibl.ientry.community.appinfo.firstaccess.user.image

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "user_image")
data class UserImage(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val id_internal_user: Long,
    val id_user: Long = 0,
    val id_drive: String? = URL_IMAGE,
    val id_access_method: Long,
    val path: String? = URL_IMAGE,
    val description: String?,
    val created_at: Date = Common.date()
){
    companion object
    {
        private val URL_IMAGE = "SIN_URL_IMAGEN"
    }
}