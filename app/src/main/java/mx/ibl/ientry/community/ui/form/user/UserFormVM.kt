package mx.ibl.ientry.community.ui.form.user

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import mx.ibl.ientry.community.appinfo.firstaccess.gender.Gender
import mx.ibl.ientry.community.appinfo.firstaccess.gender.GenderRepository
import mx.ibl.ientry.community.appinfo.firstaccess.status.Status
import mx.ibl.ientry.community.appinfo.firstaccess.status.StatusRepository
import mx.ibl.ientry.community.appinfo.firstaccess.user.User
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.util.Common
import java.util.*

class UserFormVM( app: Application, val id: Long) : AndroidViewModel( app) {

    private val userRepository: UserRepository by lazy { UserRepository(app) }
    private val genderRepository: GenderRepository by lazy { GenderRepository(app) }
    private val statusRepository: StatusRepository by lazy { StatusRepository(app) }

    val genders: LiveData<List<Gender>> = genderRepository.getAllLiveData()
    val listStatus: LiveData<List<Status>> = statusRepository.getAllLiveData()
    val user: LiveData<User> = userRepository.getUserLiveDataSafeNull( id)
    private val _genderID = MutableLiveData<Long>().apply {
        value = 0
    }
    private val _statusID = MutableLiveData<Long>().apply {
        value = 0
    }
    val genderID: LiveData<Long> = _genderID
    val statusID: LiveData<Long> = _statusID
    private val _dob = MutableLiveData<Calendar>().apply {
        value = Common.calendar()
    }
    val dob: LiveData<Calendar> = _dob


    fun setDob( data: Calendar){
        _dob.value =  data
    }

    fun setGenderID( data: Long){
        _genderID.value =  data
    }

    fun setStatusID( data: Long){
        _statusID.value =  data
    }

    fun getGenderNameByID(): String =  genderRepository.getGenderNameByID( genderID.value!!)
    fun getStatusNameByID(): String =  statusRepository.getStatusNameByID( genderID.value!!)

    fun saveChanges()
    {
        user.value?.let {
            userRepository.update(
                it.copy(
                    status_id = statusID.value!!,
                    gender_id = genderID.value!!,
                    dob = dob.value!!.time,
                    updated_at = Common.date()
                )
            )
        }
    }

}