package mx.ibl.ientry.community.ui.list.vehicle

import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemVehicleBinding

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class VehicleVH( val view: ItemVehicleBinding): RecyclerView.ViewHolder( view.root)