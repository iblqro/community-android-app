package mx.ibl.ientry.community.util.cryptography

/**
 * @author ISC Luis Cornejo
 * @since Tuesday 04, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
data class EncryptedData(
    val ciphertext: ByteArray,
    val initializationVector: ByteArray
)