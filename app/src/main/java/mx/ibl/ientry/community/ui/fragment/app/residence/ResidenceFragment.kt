package mx.ibl.ientry.community.ui.fragment.app.residence

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentResidenceBinding
import mx.ibl.ientry.community.ui.activity.main.MainVM
import mx.ibl.ientry.community.ui.list.residence.ResidenceAdapter
import mx.ibl.ientry.community.util.Common
import mx.ibl.ientry.community.util.emun.Option

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ResidenceFragment : Fragment() {

    private lateinit var binding: FragmentResidenceBinding
    private lateinit var viewModelActivity: MainVM
    private lateinit var viewModel: ResidenceVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( MainVM::class.java)
        viewModelActivity.setOption( Option.ONLY_BAR)

        viewModel = ViewModelProvider( this).get( ResidenceVM::class.java)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_residence, container, false)

        binding.itemNoData.lbIwdMessage.text = getString( R.string.lb_no_data_residency)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val adapter = ResidenceAdapter( this)
        val manager = Common.getVerticalManager( requireContext())
        val controller = AnimationUtils.loadLayoutAnimation( context, R.anim.lista_fall_down)

        binding.rvFrResidencies.layoutAnimation = controller
        binding.rvFrResidencies.adapter = adapter
        binding.rvFrResidencies.layoutManager = manager
        binding.rvFrResidencies.setHasFixedSize( true)

        lifecycleScope.launch( Dispatchers.Default){
            whenCreated {
                viewModel.listResidencies.observe( viewLifecycleOwner, {
                    if( it.isNotEmpty())
                    {
                        binding.itemNoData.root.visibility = View.GONE
                        binding.rvFrResidencies.visibility = View.VISIBLE

                        adapter.setList( it)
                        adapter.notifyDataSetChanged()
                        binding.rvFrResidencies.scheduleLayoutAnimation()
                    }
                    else
                    {
                        adapter.setList( listOf( 1L, 2L, 3L, 4L, 5L))
                        adapter.notifyDataSetChanged()
                        binding.rvFrResidencies.scheduleLayoutAnimation()
                        // binding.rvFrResidencies.visibility = View.GONE
                        // binding.itemNoData.root.visibility = View.VISIBLE
                    }
                    binding.lbFrTotal.text = String.format( getString( R.string.lb_format_total), it.size)
                })
            }
        }
    }
}