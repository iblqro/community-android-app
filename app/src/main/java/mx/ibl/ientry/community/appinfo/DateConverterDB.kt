package mx.ibl.ientry.community.appinfo

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.room.TypeConverter
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Wednesday 28, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class DateConverterDB {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}