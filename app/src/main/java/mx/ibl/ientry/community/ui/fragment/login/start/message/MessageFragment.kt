package mx.ibl.ientry.community.ui.fragment.login.start.message

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentMessageBinding
import mx.ibl.ientry.community.ui.activity.login.LogInVM


class MessageFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentMessageBinding
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var viewModel: LogInVM

    companion object{
        const val PREFERENCES_BIOMETRIC = "biometric_or_password"
        private const val TAG = "MessageFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider( requireActivity()).get( LogInVM::class.java)
        sharedPreferences = requireActivity().getSharedPreferences( getString( R.string.settings_app), Context.MODE_PRIVATE)

        requireActivity().onBackPressedDispatcher.addCallback( this){
            findNavController().popBackStack( R.id.log_in_nav, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_message, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fmMessage.btnImYes.setOnClickListener( this)
        binding.fmMessage.btnImNo.setOnClickListener( this)
        binding.imgFmBack.setOnClickListener( this)
    }

    override fun onClick(v: View?) {
        v?.let {
            when( it.id){
                binding.fmMessage.btnImYes.id ->{
                    with( sharedPreferences.edit()){
                        putBoolean( PREFERENCES_BIOMETRIC, true)
                        commit()
                    }
                    viewModel.getUserFromServer()
                    findNavController().navigate( R.id.action_messageFragment_to_log_in_nav)
                }
                binding.fmMessage.btnImNo.id ->{
                    with( sharedPreferences.edit()){
                        putBoolean( PREFERENCES_BIOMETRIC, false)
                        commit()
                    }
                    viewModel.getUserFromServer()
                    findNavController().navigate( R.id.action_messageFragment_to_log_in_nav)
                }
                binding.imgFmBack.id -> findNavController().popBackStack( R.id.log_in_nav, false)
                else -> Log.e( TAG, "Error: Not view selected")
            }
        }
    }
}