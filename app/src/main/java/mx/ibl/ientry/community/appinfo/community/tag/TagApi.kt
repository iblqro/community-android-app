package mx.ibl.ientry.community.appinfo.community.tag

import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface TagApi {

    @GET( "tag/v1/urbanization/{id}")
    fun getByUrbanizationID( @Path( "id") id: Long): Call<List<Tag>>

    @POST( "tag/v1/create")
    fun create( @Body tag: Tag): Call<Tag>

    @PUT( "tag/v1/update")
    fun update( @Body tag: Tag): Call<Tag>
}