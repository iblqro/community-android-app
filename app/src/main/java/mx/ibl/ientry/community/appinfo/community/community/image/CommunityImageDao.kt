package mx.ibl.ientry.community.appinfo.community.community.image

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Friday 30, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface CommunityImageDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert(communityImage: List<CommunityImage>): List<Long>

    @Insert
    fun insert(communityImage: CommunityImage): Long

    @Update
    fun update(communityImage: CommunityImage)

    @Query( "DELETE FROM community_image")
    fun deleteAll()

    @Query( """
        SELECT * FROM community_image WHERE urbanization_id = :id
    """)
    fun getImagesByUrbanizationID( id: Long): LiveData<List<CommunityImage>>

}