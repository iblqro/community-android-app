package mx.ibl.ientry.community.appinfo.firstaccess.user

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface UserDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( user: User): Long

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( users: List<User>): List<Long>

    @Update
    fun update( user: User)

    @Update
    fun update( users: List<User>)

    @Query( "DELETE FROM user")
    fun deleteAll()

    @Query("SELECT * FROM user WHERE is_main = 1")
    fun getMainUser(): User?

    @Query( "SELECT * FROM user WHERE internal_id = :id")
    fun getUserLiveData( id: Long): LiveData<User?>

    @Query( "SELECT * FROM user WHERE internal_id = :id")
    fun getUserLiveDataSafeNull( id: Long): LiveData<User>

    @Query(
        """
        SELECT u.internal_id FROM user u
        INNER JOIN user_residence ur ON ur.internal_user_id =  u.internal_id
        WHERE ur.user_type_id in (:types) AND ur.residence_id = :id
    """
    )
    fun getIDsUserInResidencyByIDAndCategoriesLiveData(types: List<Long>, id: Long): LiveData<List<Long>>

}