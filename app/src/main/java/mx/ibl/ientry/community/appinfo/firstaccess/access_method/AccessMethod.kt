package mx.ibl.ientry.community.appinfo.firstaccess.access_method

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "access_method")
data class AccessMethod (
    @PrimaryKey
    val id: Long,
    val name: String,
    val description: String,
    val created_at: Date = Common.date(),
    val updated_at: Date?
)