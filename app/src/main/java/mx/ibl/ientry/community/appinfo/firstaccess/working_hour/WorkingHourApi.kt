package mx.ibl.ientry.community.appinfo.firstaccess.working_hour

import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface WorkingHourApi {

    @GET( "working-hour/v1/user/{id}")
    fun getScheduledByUserID( @Path( "id") id: Long): Call<List<WorkingHour>>

    @GET( "working-hour/v1/vehicle/{id}")
    fun getScheduledByVehicleID( @Path( "id") id: Long): Call<List<WorkingHour>>

    @POST( "working-hour/v1/create")
    fun create( @Body workingHour: WorkingHour): Call<WorkingHour>

    @PUT( "working-hour/v1/update")
    fun update( @Body workingHour: WorkingHour): Call<WorkingHour>

}