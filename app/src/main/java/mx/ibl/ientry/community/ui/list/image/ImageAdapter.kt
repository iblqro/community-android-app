package mx.ibl.ientry.community.ui.list.image

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.ItemImageBinding
import mx.ibl.ientry.community.ui.viewmodel.GeneralVM

/**
 * @author ISC Luis Cornejo
 * @since Thursday 06, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ImageAdapter(val activity: ViewModelStoreOwner, val option: Int) :
    RecyclerView.Adapter<ImageVH>() {

    private var list = listOf<Long>()
    private lateinit var context: Context
    private val viewModel: GeneralVM = ViewModelProvider(activity).get(GeneralVM::class.java)

    companion object {
        const val IMAGE_RESIDENCY = 1
        const val IMAGE_VEHICLE = 2
        const val IMAGE_COMMON_AREA = 3
        const val IMAGE_URBANIZATION = 4
        const val IMAGE_PENALTY = 5
        const val IMAGE_USER = 6
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageVH {
        context = parent.context

        return ImageVH(ItemImageBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ImageVH, position: Int) {
        val id = list[position]
        val binding = holder.view


        // val intent = Intent(Intent.ACTION_VIEW)
        // intent.setDataAndType( FileProvider.getUriForFile( context,"${context.applicationContext.packageName}.provider", File( evidence.path_evidencia!!)), "image/*")
        // intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        // context.startActivity(intent)


        when (option) {
            IMAGE_RESIDENCY -> {
                viewModel.getImageResidencyByIDLiveData(id)
                    .observe(activity as LifecycleOwner, Observer {
                        it?.let { item ->
                            binding.lbIiName.text = item.description
                            binding.btnIiDelete.setOnClickListener {
                                Picasso.get().load(item.path ?: item.id_drive)
                                    .error(R.mipmap.residence)
                                val dialog = AlertDialog.Builder(context, R.style.AlertDialogTheme)
                                dialog.setTitle("Delete Residency Image")
                                    .setMessage(
                                        "Are you sure to delete this image?\n" +
                                                "You will not be able to undo the action"
                                    )
                                    .setPositiveButton(context.getString(R.string.lb_btn_accept)) { _, _ ->
                                        viewModel.deleteResidencyImage(item)
                                    }
                                    .setNegativeButton(
                                        context.getString(R.string.lb_btn_cancel),
                                        null
                                    )
                                    .create()
                                    .show()
                            }
                        }
                    })
            }
            IMAGE_VEHICLE -> {
            }
            IMAGE_COMMON_AREA -> {
            }
            IMAGE_URBANIZATION -> {
            }
            IMAGE_PENALTY -> {
            }
            IMAGE_USER -> {
            }
        }

    }

    fun setList(aux: List<Long>) {
        list = aux
    }

}