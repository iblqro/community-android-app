package mx.ibl.ientry.community.appinfo.firstaccess.status

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.network.ServiceGenerator

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class StatusRepository( context: Context) {

    private val userRepository: UserRepository by lazy { UserRepository( context)}
    private val dao: StatusDao by lazy { AppDatabase.getInstance( context).statusDao() }
    private val api: StatusApi by lazy { ServiceGenerator.createService( context, StatusApi::class.java, userRepository.token) }

    private fun insert( status: List<Status>): List<Long> = insert( status)
    fun deleteAll() = dao.deleteAll()
    fun getAllLiveData(): LiveData<List<Status>> =  dao.getAllLiveData()
    fun getStatusNameByID( id: Long): String = dao.getStatusNameByID( id)
    fun getIDStatusByName( name: String): Long = dao.getIDStatusByName( name)
}