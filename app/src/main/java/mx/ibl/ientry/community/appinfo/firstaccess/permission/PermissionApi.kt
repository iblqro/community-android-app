package mx.ibl.ientry.community.appinfo.firstaccess.permission

import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface PermissionApi {

    @GET( "permission/v1/all/{id}")
    suspend fun getAll( @Path( "id") id: Long): List<Permission>
}