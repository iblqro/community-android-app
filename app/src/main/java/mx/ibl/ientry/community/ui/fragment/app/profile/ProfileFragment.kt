package mx.ibl.ientry.community.ui.fragment.app.profile

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import androidx.navigation.fragment.findNavController
import eightbitlab.com.blurview.RenderScriptBlur
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentProfileBinding
import mx.ibl.ientry.community.ui.activity.main.MainVM
import mx.ibl.ientry.community.ui.form.user.UserFormFragment
import mx.ibl.ientry.community.util.Common
import mx.ibl.ientry.community.util.emun.Option


class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel
    private lateinit var viewModelActivity: MainVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        viewModelActivity = ViewModelProvider( requireActivity()).get(MainVM::class.java)

        viewModelActivity.setOption(Option.PROFILE)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModelActivity.setOption(Option.PROFILE)
        setBlurImage()

        lifecycleScope.launch(Dispatchers.Default){
            whenCreated {

                binding.imgFpGoBack.setOnClickListener {
                    findNavController().navigate( R.id.nav_home)
                }

               binding.imgFpLogOut.setOnClickListener {
                    AlertDialog.Builder(requireContext(), R.style.AlertDialogTheme).apply {
                        setTitle(getString(R.string.lb_mlo_log_out))
                        setMessage(getString(R.string.lb_mlo_log_out_message))
                        setPositiveButton(R.string.lb_btn_accept){ _, _ ->
                            viewModel.logOut()
                        }
                        setNegativeButton(R.string.lb_btn_cancel, null)
                        create()
                        show()
                    }
                }

                viewModel.user.observe(viewLifecycleOwner, {
                    it?.let { user ->
                        // First Name //
                        binding.lbFpFirstName.text = user.name
                        // Email //
                        binding.lbFpEmail.text = user.email
                        // Number //
                        binding.lbFpNumber.text = user.phone_number
                        // DOB //
                        binding.lbFpDob.visibility = if (user.dob == null) View.GONE else {
                            binding.lbFpDob.text = Common.dateSlashFormat.format(user.dob!!)
                            View.VISIBLE
                        }
                        // ToDo: Set Profile image and background image, is the same image //
                        binding.lbFpPersonalInformationEdit.setOnClickListener {
                            if (findNavController().currentDestination?.id != R.id.userFormFragment) {
                                val bundle = bundleOf(UserFormFragment.ID_USER to user.internal_id)
                                findNavController().navigate(
                                    R.id.action_nav_profile_to_userFormFragment,
                                    bundle
                                )
                            }
                        }
                        binding.lbFpPaymentMethod.setOnClickListener {

                        }
                        binding.lbFpPaymentHistory.setOnClickListener {

                        }
                        binding.lbFpSubscription.setOnClickListener {

                        }
                    }
                })
            }
        }
    }

    private fun setBlurImage()
    {
        val radius = 18f

        val decorView: View = requireActivity().window.decorView
        //ViewGroup you want to start blur from. Choose root as close to BlurView in hierarchy as possible.
        val rootView = decorView.findViewById<View>(android.R.id.content) as ViewGroup
        //Set drawable to draw in the beginning of each blurred frame (Optional).
        //Can be used in case your layout has a lot of transparent space and your content
        //gets kinda lost after after blur is applied.
        val windowBackground = decorView.background

        binding.bvFpContent.setupWith(rootView)
            .setFrameClearDrawable(windowBackground)
            .setBlurAlgorithm(RenderScriptBlur(requireContext()))
            .setBlurRadius(radius)
            .setBlurAutoUpdate(true)
            .setHasFixedTransformationMatrix(true)
    }


}