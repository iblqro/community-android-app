package mx.ibl.ientry.community.appinfo.community.community

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface CommunityApi {

    @GET( "urbanization/v1/{id}")
    fun get( @Path("id") id: Long): Call<Community>

    @GET( "urbanization/v1/user/{id}")
    suspend fun getByUserID( @Path( "id") id: Long): List<Community>

    @GET( "urbanization/v1/urbanization/{id_father}")
    suspend fun getUrbanizationChildren( @Path( "id_father") id: Long): List<Community>

    @PUT( "urbanization/v1/update")
    fun update( @Body community: Community): Call<Community>
}