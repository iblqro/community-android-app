package mx.ibl.ientry.community.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.community.residence.ResidenceItem
import mx.ibl.ientry.community.appinfo.community.residence.ResidenceRepository
import mx.ibl.ientry.community.appinfo.community.residence.image.ResidenceImage
import mx.ibl.ientry.community.appinfo.community.residence.image.ResidenceImageRepository
import mx.ibl.ientry.community.appinfo.firstaccess.status.StatusRepository
import mx.ibl.ientry.community.appinfo.firstaccess.user.User
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.appinfo.firstaccess.user.image.UserImage
import mx.ibl.ientry.community.appinfo.firstaccess.user.image.UserImageRepository
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.Vehicle
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.VehicleRepository
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.image.VehicleImage
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.image.VehicleImageRepository

/**
 * @author ISC Luis Cornejo
 * @since Friday 07, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class GeneralVM( app: Application): AndroidViewModel( app) {

    private val residenceRepository: ResidenceRepository by lazy { ResidenceRepository( app) }
    private val residenceImageRepository: ResidenceImageRepository by lazy { ResidenceImageRepository( app) }
    private val vehicleRepository: VehicleRepository by lazy { VehicleRepository( app) }
    private val vehicleImageRepository: VehicleImageRepository by lazy { VehicleImageRepository( app) }
    private val statusRepository: StatusRepository by lazy { StatusRepository( app) }
    private val userRepository: UserRepository by lazy { UserRepository( app) }
    private val userImageRepository: UserImageRepository by lazy { UserImageRepository( app) }


    fun getStatusNameByID( id: Long): String = statusRepository.getStatusNameByID( id)
    fun getResidencyItemByIDLiveData(id: Long): LiveData<ResidenceItem?> = residenceRepository.getResidencyItem( id)
    fun getUserByIDLiveData( id: Long): LiveData<User?> = userRepository.getUserLiveData( id)
    fun getVehicleByIDLiveData( id: Long): LiveData<Vehicle?> = vehicleRepository.getVehicleByIDLiveData( id)
    fun getImageResidencyByIDLiveData( id: Long): LiveData<ResidenceImage?> =  residenceImageRepository.getImageResidencyByIDLiveData( id)
    fun user(): User =  userRepository.user

    fun getImagesByVehicleIDLiveData( id: Long): LiveData<List<VehicleImage>> =  vehicleImageRepository.getImagesByVehicleIDLiveData( id)
    fun deleteResidencyImage(residenceImage: ResidenceImage ) = residenceImageRepository.delete( residenceImage)
    fun getImagesByResidencyIDLiveData( id: Long): LiveData<List<ResidenceImage>> =  residenceImageRepository.getImagesByResidencyIDLiveData( id)
    // ToDo: Cambiar el metodo de acceso por el correspondiente  que es obtener su imagen
    fun getImagesByUserIDLiveData( id: Long): LiveData<List<UserImage>>
        = userImageRepository.getImagesByUserIDLiveData( id, 1)
}