package mx.ibl.ientry.community.util.emun

import mx.ibl.ientry.community.R

/**
 * @author ISC Luis Cornejo
 * @since Friday 25, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
enum class Status(val key: Long, val value: Int) {

    // 0-9 USUARIOS
    STATUS_0(0, R.string.lb_status_0), // Se desactivo usuario, inactividad
    STATUS_1(1, R.string.lb_status_1), // Se autorizo
    STATUS_2(2, R.string.lb_status_2), // No se autorizo se necesitan datos extras para su prueba
    STATUS_3(3, R.string.lb_status_3), // Sólo se registro /  dio de alta (puede ser invitado que no tiene app)
    STATUS_4(4, R.string.lb_status_4), // Activo, uso normal
    STATUS_5(5, R.string.lb_status_5),// Mal uso de app / suspendido / falta de pago

    // 10-19 VEHICULOS
    STATUS_10(10, R.string.lb_status_10), // Se registro falta asociar TAG
    STATUS_11(11, R.string.lb_status_11), // TAG registrado, circula de manera adecuada
    STATUS_12(12, R.string.lb_status_12), // Por inactividad o falta de pago
    STATUS_13(13, R.string.lb_status_13), // Ya no existe esa carro en esa comunidad

    // 20-29 COMUNIDADES, RESIDENCIAS, AREAS COMUNES
    STATUS_20( 20, R.string.lb_status_20), // Se crea se necesita aprobar
    STATUS_21( 21, R.string.lb_status_21), // El registro no se aprobó, se necesitan hacer cambios para pasar a por aprobar
    STATUS_22( 22, R.string.lb_status_22), // Activa
    STATUS_23( 23, R.string.lb_status_23), // Mal uso app / falta administrativa
    STATUS_24( 24, R.string.lb_status_24), // Suspendida
    STATUS_25( 25, R.string.lb_status_25), // Se está reparando
    STATUS_26( 26, R.string.lb_status_26), // Está en mantenimiento
    // 30-39 CONSTRUCCION
    STATUS_30( 30, R.string.lb_status_30), // Se crea se necesita aprobar
    STATUS_31( 31, R.string.lb_status_31), // El registro no se aprobo, se necesitan hacer cambios para pasar a por aprobar
    STATUS_32( 32, R.string.lb_status_32), // Activa
    STATUS_33( 33, R.string.lb_status_33), // Cancelada
    STATUS_34( 34, R.string.lb_status_34), // Mal uso app / falta de pago
    STATUS_35( 35, R.string.lb_status_35),
    // 40-49 CONTRATOS
    STATUS_40( 40, R.string.lb_status_40), //
    STATUS_41( 41, R.string.lb_status_41), //
    STATUS_42( 42, R.string.lb_status_42), //
    STATUS_43( 43, R.string.lb_status_43), //
    // 50-59
    // 60-69
    // 70-79
    // 80-89
    // 90-99
    // 100-109
    // 110-119
}