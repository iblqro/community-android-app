package mx.ibl.ientry.community.appinfo.community.residence

/**
 * @author ISC Luis Cornejo
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 * @since Tuesday 04, May 2021
 */
data class ResidenceItem(
    val residence_id: Long,
    val residence: String?,
    val address: String?,
    val exterior_number: Int,
    val interior_number: Int?,
    val owner_id: Long,
    val owner: String,
    val community: String,
    val status: String,
    val no_vehicles: Int,
    val no_occupants: Int,
)