package mx.ibl.ientry.community.appinfo.firstaccess.vehicle

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.network.ServiceGenerator

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class VehicleRepository( context: Context) {

    private val userRepository: UserRepository by lazy { UserRepository( context)}
    private val dao: VehicleDao by lazy{ AppDatabase.getInstance( context).vehicleDao()}
    private val api: VehicleApi by lazy { ServiceGenerator.createService( context, VehicleApi::class.java, userRepository.token)}

    fun getIDsVehicleByResidencyIDLiveData( id: Long): LiveData<List<Long>> =  dao.getIDsVehicleByResidencyIDLiveData( id)
    fun getVehicleByIDLiveData( id: Long): LiveData<Vehicle?> = dao.getVehicleByIDLiveData( id)
}