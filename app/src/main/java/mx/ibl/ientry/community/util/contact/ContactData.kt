package mx.ibl.ientry.community.util.contact

import android.net.Uri

/**
 * @author ISC Luis Cornejo
 * @since Friday 07, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */

data class ContactData(
    val contactId: Long,
    val name: String,
    val phoneNumber: List<String>,
    val avatar: Uri?
)