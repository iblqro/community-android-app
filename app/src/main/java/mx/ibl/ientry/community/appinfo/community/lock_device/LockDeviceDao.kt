package mx.ibl.ientry.community.appinfo.community.lock_device

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface LockDeviceDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( lockDevices: List<LockDevice>): List<Long>

    @Query( "DELETE FROM lock_device")
    fun deleteAll()

    @Query( """
        SELECT * FROM lock_device WHERE id_urbanization = :id AND id_residency IS NULL
    """)
    fun getLockDevicesByUrbanizationID( id: Long): LiveData<List<LockDevice>>

    @Query( """
        SELECT * FROM lock_device WHERE id_residency = :id
    """)
    fun getLockDevicesByResidencyID( id: Long): LiveData<List<LockDevice>>
}