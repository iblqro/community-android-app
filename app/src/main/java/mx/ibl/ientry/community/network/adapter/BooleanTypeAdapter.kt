package mx.ibl.ientry.community.network.adapter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Wednesday 28, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
internal class BooleanTypeAdapter : JsonDeserializer<Boolean> {
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type,
                             context: JsonDeserializationContext?): Boolean {
        try {
            val code = json.asInt
            return if (code == 0) false else (if (code == 1) true else null)!!
        } catch (e: Exception) {
            val code1 = json.asString
            return if (code1.toLowerCase(Locale.ROOT) == "true") true else (if (code1.toLowerCase(Locale.ROOT) == "false") false else null)!!
        }
    }
}