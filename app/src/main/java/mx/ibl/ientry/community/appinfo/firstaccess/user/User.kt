package mx.ibl.ientry.community.appinfo.firstaccess.user

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "user")
data class User(
    @PrimaryKey( autoGenerate = true)
    val internal_id: Long = 0,
    val id: Long = 0,
    var status_id: Long = 0,
    var gender_id: Long = 0,
    var name: String = "",
    var password: String = "",
    var email: String = "", // to username
    var phone_number: String = "", // to username
    var dob: Date? = null,
    var country: String? = null,
    var curp: String? = null,
    var is_account_logged: String? = null,
    var created_at: Date =  Common.date(),
    var updated_at: Date? = null,
    var is_main: Boolean = false // to internal control, who is the main
)