package mx.ibl.ientry.community.appinfo.firstaccess.user.image

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface UserImageApi {

    @GET( "user-image/v1/{id}")
    fun getByUserID( @Path( "id") id: Long): Call<List<UserImage>>

    @Multipart
    @POST( "user-image/v1/create")
    fun create(
        @Part( "file") image: MultipartBody.Part,
        @Part( "user-image") userImage: UserImage
    ): Call<UserImage>
}