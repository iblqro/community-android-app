package mx.ibl.ientry.community.ui.list.residence

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.ItemResidenceBinding
import mx.ibl.ientry.community.ui.fragment.app.residence.detail.DetailResidenceFragment
import mx.ibl.ientry.community.ui.viewmodel.GeneralVM

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ResidenceAdapter(val activity: ViewModelStoreOwner): RecyclerView.Adapter<ResidenceVH>() {

    private val viewModel: GeneralVM = ViewModelProvider( activity).get( GeneralVM::class.java)
    private var list = listOf<Long>()
    private lateinit var navController: NavController
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResidenceVH {
        context = parent.context
        navController = parent.findNavController()

        return ResidenceVH( ItemResidenceBinding.inflate( LayoutInflater.from( context), parent, false))
    }

    override fun onBindViewHolder(holder: ResidenceVH, position: Int) {
        val binding = holder.view
        val id = list[position]

        binding.cvIrContainer.setOnClickListener {
            if( navController.currentDestination?.id != R.id.detailResidencyFragment)
            {
                val bundle = bundleOf( DetailResidenceFragment.ID_RESIDENCY to 0L)
                navController.navigate( R.id.action_nav_residency_to_detailResidencyFragment, bundle)
            }
        }

        /*viewModel.getResidencyItemLiveData( id).observe( activity as LifecycleOwner, Observer {
            it?.let { item ->
                viewModel.user.let { user ->
                    if( user.internal_id == item.id_owner)
                        binding.imgIrOwner.visibility = View.VISIBLE
                    else
                        binding.imgIrOwner.visibility = View.GONE
                }
                binding.lbIrName.text = item.residency
                binding.lbIrAddress.text = item.address
                binding.lbIrNumbers.text = if( item.interior_number != null) "No. Exterior: ${item.exterior_number}, Interior: ${item.interior_number}" else "No. Exterior: ${item.exterior_number}"
                binding.lbIrOwner.text = item.owner
                binding.lbIrUrbanization.text = item.urbanization
                binding.lbIrNoVehicle.text = String.format( DetailResidencyFragment.TOTAL, item.no_vehicles)
                binding.lbIrNoOccupant.text = String.format( DetailResidencyFragment.TOTAL, item.no_occupants)
                binding.lbIrStatus.text = item.status

            }
        })*/
    }

    override fun getItemCount(): Int = list.size

    fun setList( aux: List<Long>)
    {
        list = aux
    }
}