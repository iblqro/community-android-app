package mx.ibl.ientry.community.appinfo.community.restriction

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "restriction")
data class Restriction(
    @PrimaryKey
    val id: Long,
    val community_id: Long,
    val start_age_young: Int = 18,
    val end_age_young: Int = 12,
    val is_house_seed: Boolean = false,
    val no_vehicle: Int = 2,
    val work_hour_start_time: String,
    val work_minute_start_time: String,
    val work_hour_end_time: String,
    val work_minute_end_time: String,
    val work_in_saturday: Boolean = false,
    val work_in_sunday: Boolean = false,
    val day_of_inactivity: Int = 5,
    val created_at: Date,
    val updated_at:Date
)