package mx.ibl.ientry.community.appinfo.community.residence

import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface ResidenceApi {

    @GET( "residency/v1/user/{id}")
    fun getResidencyByUserID( @Path( "id") id: Long): Call<List<Residence>>

    @POST( "residency/v1/create")
    fun create( @Body residence: Residence): Call<Residence>

    @PUT( "residency/v1/update")
    fun update( @Body residence: Residence): Call<Residence>
}