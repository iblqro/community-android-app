package mx.ibl.ientry.community.appinfo.firstaccess.gender

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "gender")
data class Gender (
    @PrimaryKey
    val id: Long,
    val name: String,
    val created_at: Date,
    val update_at: Date?
)
{
    override fun toString(): String {
        return name
    }
}