package mx.ibl.ientry.community.ui.list.resident

import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemResidentBinding

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ResidentVH(val view: ItemResidentBinding): RecyclerView.ViewHolder( view.root)