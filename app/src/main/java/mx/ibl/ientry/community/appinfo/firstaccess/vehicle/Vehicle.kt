package mx.ibl.ientry.community.appinfo.firstaccess.vehicle

import androidx.room.Entity
import androidx.room.PrimaryKey
import mx.ibl.ientry.community.util.Common
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity(tableName = "vehicle")
data class Vehicle(
    // General values //
    @PrimaryKey(autoGenerate = true)
    val internal_id: Long = 0,
    val id: Long = 0,
    val id_status: Long = 0,
    val brand: String,
    val model: Int,
    val color: String,
    val plate: String,
    val serial_number: String,
    val created_at: Date = Common.date(),
    val updated_at: Date?
)