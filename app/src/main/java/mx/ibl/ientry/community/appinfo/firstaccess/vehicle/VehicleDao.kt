package mx.ibl.ientry.community.appinfo.firstaccess.vehicle

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface VehicleDao {

    @Insert
    fun insert( vehicles: List<Vehicle>): List<Long>

    @Insert
    fun insert( vehicle: Vehicle): Long

    @Update
    fun update( vehicle: Vehicle)

    @Query( "DELETE FROM vehicle")
    fun deleteAll()

    @Query( """
        SELECT v.internal_id FROM vehicle v
        INNER JOIN residence_vehicle rv ON rv.internal_vehicle_id = v.internal_id
        WHERE rv.residence_id = :id
    """)
    fun getIDsVehicleByResidencyIDLiveData( id: Long): LiveData<List<Long>>

    @Query( "SELECT * FROM vehicle WHERE internal_id = :id")
    fun getVehicleByIDLiveData( id: Long): LiveData<Vehicle?>
}