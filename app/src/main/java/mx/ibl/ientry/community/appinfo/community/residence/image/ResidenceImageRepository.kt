package mx.ibl.ientry.community.appinfo.community.residence.image

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.network.ServiceGenerator

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class ResidenceImageRepository(context: Context) {

    private val userRepository: UserRepository by lazy { UserRepository( context) }
    private val dao: ResidenceImageDao by lazy { AppDatabase.getInstance( context).residenceImageDao() }
    private val api: ResidenceImageApi by lazy { ServiceGenerator.createService( context, ResidenceImageApi::class.java, userRepository.token)}

    fun getIDsImagesByResidencyIDLiveData( id: Long): LiveData<List<Long>> = dao.getIDsImagesByResidencyIDLiveData( id)
    fun getImagesByResidencyIDLiveData( id: Long): LiveData<List<ResidenceImage>> = dao.getImagesByResidencyIDLiveData( id)
    fun getImageResidencyByIDLiveData( id: Long): LiveData<ResidenceImage?> = dao.getImageResidencyByIDLiveData( id)
    fun delete(residenceImage: ResidenceImage) = dao.delete( residenceImage)

}