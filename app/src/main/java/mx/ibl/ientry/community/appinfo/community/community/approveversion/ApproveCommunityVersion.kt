package mx.ibl.ientry.community.appinfo.community.community.approveversion

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author ISC Luis Cornejo
 * @since Thursday 24, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "approve_community_version")
data class ApproveCommunityVersion(
    @PrimaryKey
    val id: Long,
    val community_version_id: Long,
    val user_id: Long,
    val is_approve: Boolean?
)
