package mx.ibl.ientry.community.appinfo.firstaccess.user.image

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface UserImageDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( userImages: List<UserImage>): List<Long>

    @Insert
    fun insert( userImage: UserImage): Long

    @Update
    fun update( userImage: UserImage)

    @Query( "DELETE FROM user_image")
    fun deleteAll()

    @Query( """
        SELECT * FROM user_image WHERE internal_id = :id
    """)
    fun getImageByIDLiveData( id: Long): LiveData<UserImage?>

    @Query( """
        SELECT ui.internal_id FROM user_image ui
        INNER JOIN user u ON ui.id_internal_user = u.internal_id
        WHERE u.internal_id = :id AND ui.id_access_method = :idAccessMethod
    """)
    fun getIDsImageByUserIDLiveData( id: Long, idAccessMethod: Long): LiveData<List<Long>>

    @Query( """
        SELECT ui.* FROM user_image ui
        INNER JOIN user u ON ui.id_internal_user = u.internal_id
        WHERE u.internal_id = :id AND ui.id_access_method = :idAccessMethod
    """)
    fun getImagesByUserIDLiveData( id: Long, idAccessMethod: Long): LiveData<List<UserImage>>
}