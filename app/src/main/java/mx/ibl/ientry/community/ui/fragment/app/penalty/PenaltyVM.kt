package mx.ibl.ientry.community.ui.fragment.app.penalty

import android.app.Application
import androidx.lifecycle.AndroidViewModel

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class PenaltyVM( app: Application) : AndroidViewModel( app) {
}