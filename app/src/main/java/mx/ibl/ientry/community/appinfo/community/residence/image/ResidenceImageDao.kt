package mx.ibl.ientry.community.appinfo.community.residence.image

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface ResidenceImageDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert(residenceImages: List<ResidenceImage>):List<Long>

    @Insert
    fun insert(residenceImage: ResidenceImage): Long

    @Update
    fun update(residenceImage: ResidenceImage)

    @Delete
    fun delete(residenceImage: ResidenceImage)

    @Query( "DELETE FROM residency_image")
    fun deleteAll()

    @Query( "SELECT internal_id FROM residency_image WHERE id_residency = :id")
    fun getIDsImagesByResidencyIDLiveData( id: Long): LiveData<List<Long>>

    @Query( "SELECT * FROM residency_image WHERE id_residency = :id")
    fun getImagesByResidencyIDLiveData( id: Long): LiveData<List<ResidenceImage>>

    @Query( "SELECT * FROM residency_image WHERE internal_id = :id")
    fun getImageResidencyByIDLiveData( id: Long): LiveData<ResidenceImage?>
}