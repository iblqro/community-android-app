package mx.ibl.ientry.community.ui.fragment.app.community.detail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author ISC Luis Cornejo
 * @since Friday 16, July 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommunityDetailVMF( private val app: Application, private val id: Long): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if( modelClass.isAssignableFrom( CommunityDetailVM::class.java))
            return CommunityDetailVM( app, id) as T
        throw IllegalArgumentException( "Unknown CommunityDetail View Model class")
    }
}