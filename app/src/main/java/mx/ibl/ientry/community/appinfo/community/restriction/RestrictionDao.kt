package mx.ibl.ientry.community.appinfo.community.restriction

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Dao
interface RestrictionDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE)
    fun insert( restrictions: List<Restriction>): List<Long>

    @Insert
    fun insert( restriction: Restriction): Long

    @Update
    fun update( restriction: Restriction)

    @Query("SELECT * FROM restriction WHERE community_id = :id")
    fun getRestrictionByUrbanizationID( id: Long): LiveData<Restriction>

    @Query( "DELETE FROM restriction")
    fun deleteAll()
}