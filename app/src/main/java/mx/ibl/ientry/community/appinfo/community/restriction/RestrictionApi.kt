package mx.ibl.ientry.community.appinfo.community.restriction

import retrofit2.Call
import retrofit2.http.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface RestrictionApi {

    @GET( "restriction/v1/{id}")
    fun get( @Path( "id") id: Long): Call<Restriction>

    @PUT( "restriction/v1/update")
    fun update( @Body restriction: Restriction): Call<Restriction>
}