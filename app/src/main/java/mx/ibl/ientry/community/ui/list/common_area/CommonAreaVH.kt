package mx.ibl.ientry.community.ui.list.common_area

import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemCommonAreaBinding

/**
 * @author ISC Luis Cornejo
 * @since Monday 03, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommonAreaVH( val view: ItemCommonAreaBinding): RecyclerView.ViewHolder( view.root)