package mx.ibl.ientry.community.ui.fragment.app.community.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.community.community.Community
import mx.ibl.ientry.community.appinfo.community.community.CommunityRepository
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository

/**
 * @author ISC Luis Cornejo
 * @since Friday 16, July 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class CommunityDetailVM( private val app: Application, private val id: Long): AndroidViewModel( app) {

    private val userRepository: UserRepository by lazy { UserRepository( app) }
    private val communityRepository: CommunityRepository by lazy { CommunityRepository( app) }

    val community: LiveData<Community?> = communityRepository.getByIDCommunity( id)

}