package mx.ibl.ientry.community.appinfo.firstaccess.gender

import android.content.Context
import androidx.lifecycle.LiveData
import mx.ibl.ientry.community.appinfo.AppDatabase
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserRepository
import mx.ibl.ientry.community.network.ServiceGenerator

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class GenderRepository( context: Context) {

    private val userRepository: UserRepository by lazy { UserRepository( context) }
    private val dao: GenderDao by lazy { AppDatabase.getInstance( context).genderDao() }
    private val api: GenderApi by lazy { ServiceGenerator.createService( context, GenderApi::class.java, userRepository.token) }

    private fun insert( genders: List<Gender>): List<Long> = dao.insert( genders)
    fun deleteAll() = dao.deleteAll()
    fun getGenderNameByID( id: Long): String = dao.getGenderNameByID( id)
    fun getAllLiveData(): LiveData<List<Gender>> =  dao.getAllLiveData()
}