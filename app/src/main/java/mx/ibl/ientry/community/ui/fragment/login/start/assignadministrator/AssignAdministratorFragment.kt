package mx.ibl.ientry.community.ui.fragment.login.start.assignadministrator

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenCreated
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.ibl.ientry.community.R
import mx.ibl.ientry.community.databinding.FragmentAssignAdministratorBinding
import mx.ibl.ientry.community.ui.activity.login.LogInVM
import mx.ibl.ientry.community.ui.activity.main.MainActivity
import mx.ibl.ientry.community.util.Common

class AssignAdministratorFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentAssignAdministratorBinding
    private lateinit var viewModelActivity: LogInVM

    companion object{
        private const val TAG = "AssignAdministratorFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelActivity = ViewModelProvider( requireActivity()).get( LogInVM::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =  DataBindingUtil.inflate( inflater, R.layout.fragment_assign_administrator, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.faaItemAssignUserResidence.btnIaurConfirm.setOnClickListener( this)
        binding.imgFaaBack.setOnClickListener( this)

        lifecycleScope.launch( Dispatchers.Default){
            whenCreated {
                viewModelActivity.residence.observe( viewLifecycleOwner, {
                    it?.let {
                        binding.faaItemAssignUserResidence.lbIaurMessage.text = String.format( getString( R.string.lb_assign_residence), it.exterior_number, it.community_id)
                    }
                })
                viewModelActivity.residence.observe( viewLifecycleOwner, {

                })
            }
        }
    }

    private fun enterToApplication()
    {
        val intent = Intent( requireActivity(), MainActivity::class.java)
        requireActivity().startActivity( intent)
        requireActivity().overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_left)
        requireActivity().finish()
    }

    private fun fieldAreNotEmpty(): Boolean =
        binding.faaItemAssignUserResidence.iaurNewUserForm.txtNwfEmail.text.toString().isNotEmpty() &&
        binding.faaItemAssignUserResidence.iaurNewUserForm.txtNwfName.text.toString().isNotEmpty() &&
        binding.faaItemAssignUserResidence.iaurNewUserForm.txtNwfNumber.text.toString().isNotEmpty()

    override fun onClick(v: View?) {
        v?.let {
            when( it.id){
                binding.faaItemAssignUserResidence.btnIaurConfirm.id ->{
                    if( fieldAreNotEmpty())
                        if( Common.isEmailValid(  binding.faaItemAssignUserResidence.iaurNewUserForm.txtNwfEmail.text.toString()))
                            if( viewModelActivity.residences.value!!.size - 1 > 0)
                                findNavController().navigate( R.id.action_assignAdministratorFragment_to_manageResidenceFragment)
                            else
                                enterToApplication()
                        else
                            Common.showSnackBar( binding.root, getString( R.string.lb_email_not_valid), requireContext())
                    else
                        Common.showSnackBar( binding.root, getString( R.string.lb_warning_data_empty), requireContext())
                }
                binding.imgFaaBack.id -> findNavController().popBackStack()
                else -> Log.e( TAG, "ID: ${it.id} Unknown")
            }
        }
    }
}