package mx.ibl.ientry.community.appinfo

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import mx.ibl.ientry.community.appinfo.AppDatabase.Companion.DB_NAME
import mx.ibl.ientry.community.appinfo.community.catalog.residence_vehicle.ResidenceVehicle
import mx.ibl.ientry.community.appinfo.community.catalog.user_community.UserCommunity
import mx.ibl.ientry.community.appinfo.community.catalog.user_residence.UserResidence
import mx.ibl.ientry.community.appinfo.community.common_area.CommonArea
import mx.ibl.ientry.community.appinfo.community.common_area.CommonAreaDao
import mx.ibl.ientry.community.appinfo.community.common_area.image.CommonAreaImage
import mx.ibl.ientry.community.appinfo.community.common_area.image.CommonAreaImageDao
import mx.ibl.ientry.community.appinfo.community.community.Community
import mx.ibl.ientry.community.appinfo.community.community.CommunityDao
import mx.ibl.ientry.community.appinfo.community.community.approveversion.ApproveCommunityVersion
import mx.ibl.ientry.community.appinfo.community.community.communityversion.CommunityVersion
import mx.ibl.ientry.community.appinfo.community.community.image.CommunityImage
import mx.ibl.ientry.community.appinfo.community.community.image.CommunityImageDao
import mx.ibl.ientry.community.appinfo.community.community.worker.WorkerCommunity
import mx.ibl.ientry.community.appinfo.community.lock_device.LockDevice
import mx.ibl.ientry.community.appinfo.community.lock_device.LockDeviceDao
import mx.ibl.ientry.community.appinfo.community.residence.ResidenceDao
import mx.ibl.ientry.community.appinfo.community.residence.Residence
import mx.ibl.ientry.community.appinfo.community.residence.image.ResidenceImage
import mx.ibl.ientry.community.appinfo.community.residence.image.ResidenceImageDao
import mx.ibl.ientry.community.appinfo.community.restriction.Restriction
import mx.ibl.ientry.community.appinfo.community.restriction.RestrictionDao
import mx.ibl.ientry.community.appinfo.community.tag.Tag
import mx.ibl.ientry.community.appinfo.community.tag.TagDao
import mx.ibl.ientry.community.appinfo.firstaccess.access_method.AccessMethod
import mx.ibl.ientry.community.appinfo.firstaccess.access_method.AccessMethodDao
import mx.ibl.ientry.community.appinfo.firstaccess.authorization.Authorization
import mx.ibl.ientry.community.appinfo.firstaccess.authorization.AuthorizationDao
import mx.ibl.ientry.community.appinfo.firstaccess.day.Day
import mx.ibl.ientry.community.appinfo.firstaccess.day.DayDao
import mx.ibl.ientry.community.appinfo.firstaccess.gender.Gender
import mx.ibl.ientry.community.appinfo.firstaccess.gender.GenderDao
import mx.ibl.ientry.community.appinfo.firstaccess.permission.Permission
import mx.ibl.ientry.community.appinfo.firstaccess.permission.PermissionDao
import mx.ibl.ientry.community.appinfo.firstaccess.status.Status
import mx.ibl.ientry.community.appinfo.firstaccess.status.StatusDao
import mx.ibl.ientry.community.appinfo.firstaccess.user.User
import mx.ibl.ientry.community.appinfo.firstaccess.user.UserDao
import mx.ibl.ientry.community.appinfo.firstaccess.user.image.UserImage
import mx.ibl.ientry.community.appinfo.firstaccess.user.image.UserImageDao
import mx.ibl.ientry.community.appinfo.firstaccess.user.type.UserType
import mx.ibl.ientry.community.appinfo.firstaccess.user.type.UserTypeDao
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.Vehicle
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.VehicleDao
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.image.VehicleImage
import mx.ibl.ientry.community.appinfo.firstaccess.vehicle.image.VehicleImageDao
import mx.ibl.ientry.community.appinfo.firstaccess.work_type.WorkType
import mx.ibl.ientry.community.appinfo.firstaccess.work_type.WorkTypeDao
import mx.ibl.ientry.community.appinfo.firstaccess.working_hour.WorkingHour
import mx.ibl.ientry.community.appinfo.firstaccess.working_hour.WorkingHourDao

/**
 * @author ISC Luis Cornejo
 * @since Wednesday 28, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Database(
    entities =
    [
        // First Access //
        AccessMethod::class,
        Authorization::class,
        Day::class,
        Gender::class,
        Permission::class,
        Status::class,
        User::class,
        UserImage::class,
        UserType::class,
        Vehicle::class,
        VehicleImage::class,
        WorkingHour::class,
        WorkType::class,
        // Community //
        CommonArea::class,
        CommonAreaImage::class,
        LockDevice::class,
        Residence::class,
        ResidenceImage::class,
        ResidenceVehicle::class,
        Restriction::class,
        Tag::class,
        Community::class,
        CommunityImage::class,
        UserCommunity::class,
        UserResidence::class,
        WorkerCommunity::class,
        CommunityVersion::class,
        ApproveCommunityVersion::class,
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters(DateConverterDB::class)
abstract class AppDatabase : RoomDatabase() {

    // First Access DAO´s //
    abstract fun accessMethodDao(): AccessMethodDao
    abstract fun authorizationDao(): AuthorizationDao
    abstract fun dayDao(): DayDao
    abstract fun genderDao(): GenderDao
    abstract fun permissionDao(): PermissionDao
    abstract fun statusDao(): StatusDao
    abstract fun userDao(): UserDao
    abstract fun userImageDao(): UserImageDao
    abstract fun userTypeDao(): UserTypeDao
    abstract fun vehicleDao(): VehicleDao
    abstract fun vehicleImageDao(): VehicleImageDao
    abstract fun workingHourDao(): WorkingHourDao
    abstract fun workTypeDao(): WorkTypeDao

    // Community DAO´s //
    abstract fun commonAreaDao(): CommonAreaDao
    abstract fun commonAreaImageDao(): CommonAreaImageDao
    abstract fun lockDeviceDao(): LockDeviceDao
    abstract fun residenceDao(): ResidenceDao
    abstract fun residenceImageDao(): ResidenceImageDao
    abstract fun restrictionDao(): RestrictionDao
    abstract fun tagDao(): TagDao
    abstract fun communityDao(): CommunityDao
    abstract fun communityImageDao(): CommunityImageDao

    companion object : SingletonHolderDB<AppDatabase, Context>({
        Room.databaseBuilder(it.applicationContext, AppDatabase::class.java, DB_NAME)
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }) {
        const val DB_NAME = "ientry"
    }
}