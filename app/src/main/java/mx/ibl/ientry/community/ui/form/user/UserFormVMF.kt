package mx.ibl.ientry.community.ui.form.user

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

/**
 * @author ISC Luis Cornejo
 * @since Tuesday 11, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class UserFormVMF( private val app: Application,  private val id: Long): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if( modelClass.isAssignableFrom( UserFormVM::class.java))
            return UserFormVM( app, id) as T

        throw IllegalArgumentException( "Unknown UserForm ViewModel class")
    }
}