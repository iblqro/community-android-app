package mx.ibl.ientry.community.appinfo.firstaccess.user.type

import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
interface UserTypeApi {

    @GET( "user-type/v1/all/{id}")
    fun getAll( @Path( "id") id:Long): List<UserType>
}