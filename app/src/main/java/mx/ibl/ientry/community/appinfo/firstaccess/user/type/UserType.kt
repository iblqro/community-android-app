package mx.ibl.ientry.community.appinfo.firstaccess.user.type

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

/**
 * @author ISC Luis Cornejo
 * @since Thursday 29, April 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
@Entity( tableName = "user_type")
data class UserType(
    @PrimaryKey
    val id: Long,
    val name: String,
    val description: String,
    val id_solution: Int,
    val created_at: Date,
    val updated_at: Date?,
    val is_owner: Boolean = false
){
    companion object{
       private const val YOUNGER = 1L
       private const val MAIN_OCCUPANT = 2L
       private const val BASIC_OCCUPANT = 3L
       private const val SPECIAL_OCCUPANT = 4L
       private const val RESIDENCY_WORKER = 5L

        val listOfResidencyUsers = listOf( YOUNGER, MAIN_OCCUPANT, BASIC_OCCUPANT, SPECIAL_OCCUPANT)
        val listOfWorkersResidency = listOf( RESIDENCY_WORKER)
    }
}