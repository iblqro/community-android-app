package mx.ibl.ientry.community.ui.list.vote

import androidx.recyclerview.widget.RecyclerView
import mx.ibl.ientry.community.databinding.ItemUserVoteBinding

/**
 * @author ISC Luis Cornejo
 * @since Friday 25, June 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class VoteVH( val view: ItemUserVoteBinding): RecyclerView.ViewHolder( view.root)