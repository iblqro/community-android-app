package mx.ibl.ientry.community.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

/**
 * @author ISC Luis Cornejo
 * @since Tuesday 04, May 2021
 * @enterprise Intelligence Bereau and Laboratory SA de CV
 */
class PermissionInterceptor(private val token: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder()
                .header("Content-Type", "application/json;charset=utf-8")
                .header("Authorization", "Bearer $token")
        val request = builder.build()
        return chain.proceed(request)
    }
}